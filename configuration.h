#ifndef CONFIGURATION_H
#define CONFIGURATION_H



#include <QString>
#include <QSettings>
#include <QStringBuilder>
#include <QCoreApplication>
#include <QMessageBox>
#include <QHostInfo>
#include <windows.h>
#include <QProcess>
#include <QtNetwork/QHostInfo>




class Configuration
{
public:
    Configuration();

    /*
      GETTEUR BASE DE DONNEE CONFIG.INI
      */
    QString getDatabase();
    QString getHostName();
    QString getUserName();
    QString getPassWord();
    QString getDataBaseName();
    QString getUserCourant();
    QString getPathINI();
    bool getCrashSuperViseur();

    QStringList getListKey(QString fichierIni, QString groupe);
    QString getValue(QString fichierIni, QString groupe, QString key);
};

#endif // CONFIGURATION_H
