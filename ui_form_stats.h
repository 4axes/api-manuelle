/********************************************************************************
** Form generated from reading UI file 'form_stats.ui'
**
** Created: Thu 17. Mar 09:32:34 2016
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_STATS_H
#define UI_FORM_STATS_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCalendarWidget>
#include <QtGui/QCheckBox>
#include <QtGui/QFrame>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QRadioButton>
#include <QtGui/QWidget>
#include "qcustomplot.h"

QT_BEGIN_NAMESPACE

class Ui_Form_stats
{
public:
    QLabel *label_titre;
    QCalendarWidget *calendrier;
    QCustomPlot *widget_stats;
    QRadioButton *radioButton_rechercheAuto;
    QRadioButton *radioButton_rechercheManu;
    QLineEdit *lineEdit_dateDebut;
    QLineEdit *lineEdit_dateFin;
    QLabel *label;
    QPushButton *btn_genererStats;
    QFrame *line;
    QLabel *label_apiAuto;
    QLabel *label_apiManu;
    QRadioButton *radioButton_reconciliationAuto;
    QRadioButton *radioButton_reconciliationManu;
    QCheckBox *checkBox_journee;
    QCheckBox *checkBox_periode;
    QLabel *label_moyenne;
    QLineEdit *lineEdit_moyenne;
    QPushButton *btn_exporter;

    void setupUi(QWidget *Form_stats)
    {
        if (Form_stats->objectName().isEmpty())
            Form_stats->setObjectName(QString::fromUtf8("Form_stats"));
        Form_stats->resize(955, 761);
        label_titre = new QLabel(Form_stats);
        label_titre->setObjectName(QString::fromUtf8("label_titre"));
        label_titre->setGeometry(QRect(390, 20, 131, 16));
        calendrier = new QCalendarWidget(Form_stats);
        calendrier->setObjectName(QString::fromUtf8("calendrier"));
        calendrier->setGeometry(QRect(30, 110, 256, 155));
        widget_stats = new QCustomPlot(Form_stats);
        widget_stats->setObjectName(QString::fromUtf8("widget_stats"));
        widget_stats->setGeometry(QRect(20, 290, 901, 421));
        radioButton_rechercheAuto = new QRadioButton(Form_stats);
        radioButton_rechercheAuto->setObjectName(QString::fromUtf8("radioButton_rechercheAuto"));
        radioButton_rechercheAuto->setGeometry(QRect(540, 160, 71, 17));
        radioButton_rechercheManu = new QRadioButton(Form_stats);
        radioButton_rechercheManu->setObjectName(QString::fromUtf8("radioButton_rechercheManu"));
        radioButton_rechercheManu->setGeometry(QRect(670, 160, 81, 17));
        lineEdit_dateDebut = new QLineEdit(Form_stats);
        lineEdit_dateDebut->setObjectName(QString::fromUtf8("lineEdit_dateDebut"));
        lineEdit_dateDebut->setGeometry(QRect(320, 140, 113, 20));
        lineEdit_dateFin = new QLineEdit(Form_stats);
        lineEdit_dateFin->setObjectName(QString::fromUtf8("lineEdit_dateFin"));
        lineEdit_dateFin->setGeometry(QRect(320, 190, 113, 20));
        label = new QLabel(Form_stats);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(360, 170, 21, 16));
        btn_genererStats = new QPushButton(Form_stats);
        btn_genererStats->setObjectName(QString::fromUtf8("btn_genererStats"));
        btn_genererStats->setGeometry(QRect(700, 260, 121, 23));
        line = new QFrame(Form_stats);
        line->setObjectName(QString::fromUtf8("line"));
        line->setGeometry(QRect(640, 130, 20, 81));
        line->setFrameShape(QFrame::VLine);
        line->setFrameShadow(QFrame::Sunken);
        label_apiAuto = new QLabel(Form_stats);
        label_apiAuto->setObjectName(QString::fromUtf8("label_apiAuto"));
        label_apiAuto->setGeometry(QRect(540, 130, 91, 20));
        label_apiManu = new QLabel(Form_stats);
        label_apiManu->setObjectName(QString::fromUtf8("label_apiManu"));
        label_apiManu->setGeometry(QRect(680, 130, 71, 20));
        radioButton_reconciliationAuto = new QRadioButton(Form_stats);
        radioButton_reconciliationAuto->setObjectName(QString::fromUtf8("radioButton_reconciliationAuto"));
        radioButton_reconciliationAuto->setGeometry(QRect(540, 190, 91, 17));
        radioButton_reconciliationManu = new QRadioButton(Form_stats);
        radioButton_reconciliationManu->setObjectName(QString::fromUtf8("radioButton_reconciliationManu"));
        radioButton_reconciliationManu->setGeometry(QRect(670, 190, 91, 17));
        checkBox_journee = new QCheckBox(Form_stats);
        checkBox_journee->setObjectName(QString::fromUtf8("checkBox_journee"));
        checkBox_journee->setGeometry(QRect(100, 80, 70, 17));
        checkBox_periode = new QCheckBox(Form_stats);
        checkBox_periode->setObjectName(QString::fromUtf8("checkBox_periode"));
        checkBox_periode->setGeometry(QRect(180, 80, 70, 17));
        label_moyenne = new QLabel(Form_stats);
        label_moyenne->setObjectName(QString::fromUtf8("label_moyenne"));
        label_moyenne->setGeometry(QRect(520, 260, 61, 16));
        lineEdit_moyenne = new QLineEdit(Form_stats);
        lineEdit_moyenne->setObjectName(QString::fromUtf8("lineEdit_moyenne"));
        lineEdit_moyenne->setGeometry(QRect(580, 260, 113, 20));
        btn_exporter = new QPushButton(Form_stats);
        btn_exporter->setObjectName(QString::fromUtf8("btn_exporter"));
        btn_exporter->setGeometry(QRect(830, 260, 91, 23));

        retranslateUi(Form_stats);

        QMetaObject::connectSlotsByName(Form_stats);
    } // setupUi

    void retranslateUi(QWidget *Form_stats)
    {
        Form_stats->setWindowTitle(QApplication::translate("Form_stats", "Form", 0, QApplication::UnicodeUTF8));
        label_titre->setText(QApplication::translate("Form_stats", "Statistique r\303\251conciliation", 0, QApplication::UnicodeUTF8));
        radioButton_rechercheAuto->setText(QApplication::translate("Form_stats", "Recherche", 0, QApplication::UnicodeUTF8));
        radioButton_rechercheManu->setText(QApplication::translate("Form_stats", "Recherche", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("Form_stats", "Au", 0, QApplication::UnicodeUTF8));
        btn_genererStats->setText(QApplication::translate("Form_stats", "G\303\251n\303\251rer statistique", 0, QApplication::UnicodeUTF8));
        label_apiAuto->setText(QApplication::translate("Form_stats", "API Automatique", 0, QApplication::UnicodeUTF8));
        label_apiManu->setText(QApplication::translate("Form_stats", "API Manuelle", 0, QApplication::UnicodeUTF8));
        radioButton_reconciliationAuto->setText(QApplication::translate("Form_stats", "R\303\251conciliation", 0, QApplication::UnicodeUTF8));
        radioButton_reconciliationManu->setText(QApplication::translate("Form_stats", "R\303\251conciliation", 0, QApplication::UnicodeUTF8));
        checkBox_journee->setText(QApplication::translate("Form_stats", "Journ\303\251e", 0, QApplication::UnicodeUTF8));
        checkBox_periode->setText(QApplication::translate("Form_stats", "P\303\251riode", 0, QApplication::UnicodeUTF8));
        label_moyenne->setText(QApplication::translate("Form_stats", "Moyenne : ", 0, QApplication::UnicodeUTF8));
        btn_exporter->setText(QApplication::translate("Form_stats", "Exporter", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class Form_stats: public Ui_Form_stats {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_STATS_H
