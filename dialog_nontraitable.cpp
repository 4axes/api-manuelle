#include "dialog_nontraitable.h"
#include "ui_dialog_nontraitable.h"

Dialog_nonTraitable::Dialog_nonTraitable(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Dialog_nonTraitable)
{
    ui->setupUi(this);
    this->setFixedSize (361,363);
    this->setWindowTitle ("Motif non traitable");

    ui->textEdit->setVisible ( false );

}

Dialog_nonTraitable::~Dialog_nonTraitable()
{
    delete ui;
}






void Dialog_nonTraitable::on_btn_valider_clicked(){

    bool siMotif = true;

    if( ui->radioButton_autres->isChecked () )
        emit on_motifValider ( ui->textEdit->toPlainText (), ui->checkBox_nonEnvoi->isChecked () );
    else{

        if ( ui->radioButton_ChManquant->isChecked () )
            emit on_motifValider ( ui->radioButton_ChManquant->text (), ui->checkBox_nonEnvoi->isChecked () );

        else if ( ui->radioButton_dateEntreeManquante->isChecked () )
            emit on_motifValider ( ui->radioButton_dateEntreeManquante->text (), ui->checkBox_nonEnvoi->isChecked () );

        else if ( ui->radioButton_inforsManquantes->isChecked () )
            emit on_motifValider ( ui->radioButton_inforsManquantes->text (), ui->checkBox_nonEnvoi->isChecked () );

        else if ( ui->radioButton_mauvaisCH->isChecked () )
            emit on_motifValider ( ui->radioButton_mauvaisCH->text (), ui->checkBox_nonEnvoi->isChecked () );

        else if ( ui->radioButton_retourDPEC->isChecked () )
            emit on_motifValider ( ui->radioButton_retourDPEC->text (), ui->checkBox_nonEnvoi->isChecked () );

        else if ( ui->radioButton_retourIllisible->isChecked () )
            emit on_motifValider ( ui->radioButton_retourIllisible->text (), ui->checkBox_nonEnvoi->isChecked () );

        else if ( ui->radioButton_rpecCoupee->isChecked () )
            emit on_motifValider ( ui->radioButton_rpecCoupee->text (), ui->checkBox_nonEnvoi->isChecked () );

        else
            siMotif = false;

    }

    if( siMotif )
        close();
    else
        QMessageBox::warning (NULL,"Motif NT","Veuillez sélectionner un motif pour déclarer votre non traitable");
}





void Dialog_nonTraitable::on_btn_annuler_clicked(){

    close();
}





void Dialog_nonTraitable::on_radioButton_autres_clicked(){

    ui->textEdit->setVisible ( true );

}

