#include "Dial_bdd.h"

Dial_bdd::Dial_bdd(QString baseName){

    db = QSqlDatabase::addDatabase( config->getDatabase() );
    db.setHostName( config->getHostName() );
    db.setUserName( config->getUserName() );
    db.setPassword( config->getPassWord() );

    bddBaseName = baseName;
    db.setDatabaseName( bddBaseName );
}


//----------------------------------------------------------------------------------------------------------------------
/*
  Met � jour la base de donn�es
  Param�tre entr�e :
    > Nom de l'opratrice en cours de connexion
    > contexte (colonne)
    > stats (valeur)
    */
void Dial_bdd::majDonnee (QString user, QString contexte, int stats){

    if( ! db.open() )
        QMessageBox::warning(NULL,"Connexion base de donn�es","Impossible se connecter � la base de donn�es "
                             " - Veuillez v�rifier vos param�tres de connexion " % db.lastError().text());
    else{

        QSqlQuery sqlQuery;
        int nombre = 0;
        int total = 0;

        //Requ�te SELECT colonne NBNR TOTAL
        QString requete = "SELECT " % contexte % ", TOTAL FROM TRACE WHERE NOMOPE='" % user % "' AND SESSION='En cours';";

        //On r�cup�re les valeurs actuelle
        if( sqlQuery.exec (requete) ){

             while( sqlQuery.next () ){

                 nombre = sqlQuery.value (0).toString ().toInt ();
                 total = sqlQuery.value (1).toString ().toInt ();

                 //On incr�mente
                 nombre += stats;
                 total += stats;
             }
             sqlQuery.clear ();


            //Requ�te mise � jour
            requete =  "UPDATE TRACE SET " % contexte % " = '" % QString::number (nombre)
                    % "', TOTAL = '" % QString::number (total)
                    % "' WHERE NOMOPE = '" % user % "' AND SESSION = 'En cours'";

            if( !sqlQuery.exec ( requete ) )
                debuggerInfo("Ex�cution requ�te","Impossible d'ex�cuter la requ�te [ " % requete % " ] "
                             % " | Erreur : " % sqlQuery.lastError ().text () );


            //Fermeture de la base de donn�es
            db.commit();
            db.removeDatabase ( bddBaseName );
            db.close();

        }
        else
            debuggerInfo ("Erreur ex�cution requ�te", "Impossible d'ex�cuter la requ�te [ " % requete % " ] | Erreur "
                          % sqlQuery.lastError ().text () );
    }
}



















//----------------------------------------------------------------------------------------------------------------------
/*
  Permet d'ins�rer une ligne de donn�es dans la table tbl_erreurDest
  */
void Dial_bdd::ajouterErreurDest(QString user, int stats){


    if( ! db.open() )
        QMessageBox::warning(NULL,"Connexion base de donn�es","Impossible se connecter � la base de donn�es "
                             " - Veuillez v�rifier vos param�tres de connexion " % db.lastError().text());
    else{

        QSqlQuery sqlCreateTable = db.exec("CREATE TABLE IF NOT EXISTS tbl_erreurDest (NOMOPE VARCHAR(20), VM VARCHAR(20),"
                                           "DATE_HEURE VARCHAR(20), MUTUELLE_PREV VARCHAR (255), MUTUELLE_SUIV VARCHAR(255),"
                                           ";");

        QSqlQuery sqlQuery;

        //Requ�te SELECT colonne NBNR TOTAL
//        QString requete = "INSERT INTO tbl_erreurDest " % contexte % ", TOTAL FROM TRACE WHERE NOMOPE='" % user % "' AND SESSION='En cours';";

    }
}


























//----------------------------------------------------------------------------------------------------------------------
/*
  Permet d'importer les donn�es depuis la base de donn�es des statistiques
  */
QVector<double> Dial_bdd::importerStats(QString contexte, QString typeAPI, QString date){

    if( ! db.open() )
        QMessageBox::warning(NULL,"Connexion base de donn�es","Impossible se connecter � la base de donn�es "
                             " - Veuillez v�rifier vos param�tres de connexion " % db.lastError().text());
    else{

        QSqlQuery sqlQuery;
        QString requete;

        QVector<double> vecteurDonnee;

        requete = "SELECT temps FROM tbl_stats WHERE typeAPI='" % typeAPI % "' AND date='" % date % "' AND contexte='" % contexte % "';";

        if( !sqlQuery.exec ( requete ) )
            debuggerInfo ("Erreur requ�te", "La requ�te [ " % requete % " ] n'a pas pu s'ex�cuter | Erreur : " % sqlQuery.lastError ().text () );

        else{

            //On r�uni les valeurs
            while( sqlQuery.next () ){

                vecteurDonnee.push_back (sqlQuery.value (0).toString ().toFloat ()/ 1000);
            }
            sqlQuery.clear ();

            //Cloture de la connexion
            db.commit();
            db.removeDatabase ( bddBaseName  );
            db.close();

            return( vecteurDonnee );
        }
    }
}



















//----------------------------------------------------------------------------------------------------------------------
/*
  Permet d'importer les donn�es depuis la base de donn�es des statistiques
  */
QVector<double> Dial_bdd::importerStats(QString contexte, QString typeAPI, QString dateDebut, QString dateFin){


    if( ! db.open() )
        QMessageBox::warning(NULL,"Connexion base de donn�es","Impossible se connecter � la base de donn�es "
                             " - Veuillez v�rifier vos param�tres de connexion " % db.lastError().text());
    else{

        QSqlQuery sqlQuery;
        QString requete;

        QVector<double> vecteurDonnee;

        requete = "SELECT temps FROM tbl_stats WHERE typeAPI='" % typeAPI % "' AND contexte='" % contexte % "'"
                " AND date>='" % dateDebut % "' AND date<='" % dateFin % "';";

        if( !sqlQuery.exec ( requete ) )
            debuggerInfo ("Erreur requ�te", "La requ�te [ " % requete % " ] n'a pas pu s'ex�cuter | Erreur : " % sqlQuery.lastError ().text () );

        else{

            //On r�uni les valeurs
            while( sqlQuery.next () ){

                vecteurDonnee.push_back (sqlQuery.value (0).toString ().toFloat () / 1000);
            }
            sqlQuery.clear ();


            //Cloture de la connexion
            db.commit();
            db.removeDatabase ( bddBaseName  );
            db.close();

            return( vecteurDonnee );
        }
    }
}





















//----------------------------------------------------------------------------------------------------------------------
void Dial_bdd::debuggerInfo(QString titre, QString msg){

    QMessageBox::information (NULL,titre, msg);
}


























//----------------------------------------------------------------------------------------------------------------------
/*
  Enregistre la date de r�conciliation [date], le numero de s�curit� social [nni] et le temps de l'envoi du fichier [temps]
  */
void Dial_bdd::enregistrerStats(QString date, QString contexte, QString api, QString temps){


    if( ! db.open() )
        QMessageBox::warning(NULL,"Connexion base de donn�es","Impossible se connecter � la base de donn�es "
                             " - Veuillez v�rifier vos param�tres de connexion " % db.lastError().text());
    else{

        QSqlQuery sqlQuery;
        QString requete;

        QSqlQuery sqlCreateTable = db.exec("CREATE TABLE IF NOT EXISTS tbl_stats (date VARCHAR(20), contexte VARCHAR(20),"
                                           "  typeAPI VARCHAR(20), temps VARCHAR(20));");

        requete = "INSERT INTO tbl_stats VALUES('" % date % "', '" % contexte % "', '" % api % "', '" % temps % "');";

        if( !sqlQuery.exec ( requete ) )
            debuggerInfo ("Erreur requ�te", "La requ�te [ " % requete % " ] n'a pas pu s'ex�cuter | Erreur : " % sqlQuery.lastError ().text () );

        else{

            sqlQuery.clear ();


            //Cloture de la connexion
            db.commit();
            db.removeDatabase ( bddBaseName  );
            db.close();
        }
    }
}









//----------------------------------------------------------------------------------------------------------------------
/*
  Permet d'enregistrer certaines informations concernant la DPEC
  Nous permettra de savoir d'ou provient la r�conciliation
  */
void Dial_bdd::enregistrerReconciliation(QString nomFichier, QString idMessage, QString operatrice, QString vm, QString action, QString nomP,
                                         QString prenomP, QString numDossier, QString dateE, QString CH, QString mutuelleDpec,
                                         QString mutuelleVm){

    if( ! db.open() )
        QMessageBox::warning(NULL,"Connexion base de donn�es","Impossible se connecter � la base de donn�es "
                             " - Veuillez v�rifier vos param�tres de connexion " % db.lastError().text());
    else{

        QSqlQuery sqlQuery;
        QString requete;
        QDateTime dateTime;

        /*
            Permet de supprimuer le '\' et les apostrophe -> fait planter l'insertion en bdd
            */

        CH.replace ("\"","");
        CH.replace ("'"," ");

        mutuelleDpec.replace ("\"","");
        mutuelleDpec.replace ("'","");

        nomP.replace ("\"","");
        nomP.replace ("'","");

        prenomP.replace ("\"","");
        prenomP.replace ("'","");




        db.exec("CREATE TABLE IF NOT EXISTS tbl_messageReconcilie ("
                                           " nomFichier VARCHAR(100),"
                                           " idMessage VARCHAR(50),"
                                           " dateReconciliation VARCHAR(20),"
                                           " heureReconciliation VARCHAR(10),"
                                           " nomOperatrice VARCHAR(20),"
                                           " vm VARCHAR(20),"
                                           " action VARCHAR(255),"
                                           " nomPatient VARCHAR(255),"
                                           " prenomPatient VARCHAR(255),"
                                           " numDossier VARCHAR(255),"
                                           " dateEntree VARCHAR(10),"
                                           " CH VARCHAR (255),"
                                           " mutuelleDpec VARCHAR(255),"
                                           " mutuelleVm VARCHAR(255));");

        requete = "INSERT INTO tbl_messageReconcilie VALUES('" % nomFichier
                % "', '" % idMessage
                % "', '" % dateTime.currentDateTime ().toString ("yyyyMMdd")
                % "', '" % dateTime.currentDateTime ().toString ("hh:mm:ss")
                % "', '" % QString::fromUtf8 ( operatrice.toAscii ().constData () )
                % "', '" % vm
                % "', '" % action
                % "', '" % QString::fromUtf8 ( nomP.toAscii ().constData () )
                % "', '" % QString::fromUtf8 ( prenomP.toAscii ().constData () )
                % "', '" % numDossier
                % "', '" % dateE
                % "', '\"" % CH
                % "\"', '" % mutuelleDpec
                % "', '" % mutuelleVm % "');";

        if( !sqlQuery.exec ( requete) )
            QMessageBox::warning (NULL,"Ex�cution requ�te","Impossible d'ex�cuter la requ�te [ " % requete % " ] |"
                                  " Erreur : " % sqlQuery.lastError ().text () );

        else{

            sqlQuery.clear ();

            //Cloture de la connexion
            db.commit();
            db.removeDatabase ( bddBaseName  );
            db.close();
        }
    }
}















