#include "mainwindow.h"
#include "ui_mainwindow.h"



/************************************************************************
  DEVELOPPEUR : VINCENT PARRAMON
  DATE : 18/05/2015

  Description :

        Cette application permet de dialoguer avec un web service
        bas� sur la m�thode REST.

        Ce logiciel intervient dans la partie r�conciliation manuelle
        effectu�e par les op�ratrices de saisies

        Param�tre entrant : path complet du fichier PDF s�lectionn�

************************************************************************/

#define path_test                       QString("C:\\Users\\4AXES\\Desktop\\PoperFax\\GoodNIN\\504668278_PROBTP_FLOIRAC\\ERREUR")
//#define path_test                       QString("C:\\Users\\4AXES\\Desktop\\PoperFax\\UnknownNIN\\504668278_PROBTP_FLOIRAC")

//CONSTANTE
#define NOM_FICHIER_RECEPTION           QString("retour.pdf")
#define NOM_FICHIER_LOG                 QString("reconciliation.log")
#define NOM_FICHIER_RECEPTIONEDS        QString("eds.csv")
#define HTTP_DOMAINE_4AXES              QString("domaine")
#define HTTP_DOMAINE_4AXES_VALEUR       QString("4axes")
#define MOT_DE_PASSE_SWITCH             QString("mejpm")

//Erreur
#define ERREUR_RECONCILIATION_01        QString("Veuillez recommencer votre r�conciliation et v�rifier les informations")
#define ERREUR_RECONCILIATION_02        QString("Veuillez v�rifier le RNM de la mutuelle sur la plateforme")
#define ERREUR_RECHERCHE                QString("Erreur lors de la recherche (getNNI)")


//Serveur de test local
//#define ADR_HTTP_RECONCILIER        QString("http://192.168.1.12/transmissions/httpsdocs/api/reconcillier")
//#define ADR_HTTP_GETNNI             QString("http://192.168.1.12/transmissions/httpsdocs/api/getnni")
//#define ADR_HTTP_ENVOYER_SEC        QString("http://192.168.1.12/transmissions/httpsdocs/api/envoisec")
//#define ADR_HTTP_DOWNLOAD           QString("http://192.168.1.12/transmissions/httpsdocs/api/download")
//#define ADR_HTTP_RECHERCHE          QString("http://192.168.1.12/transmissions/httpsdocs/api/recherche")
//#define ADR_HTTP_GETMESSAGE         QString("http://192.168.1.12/transmissions/httpsdocs/api/getmessage")
//#define ADR_HTTP_DOWNLOAD_EDS       QString("http://192.168.1.12/transmissions/httpsdocs/api/geteds")
//#define ADR_HTTP_ENVOYER_SWITCH     QString("http://192.168.1.12/transmissions/httpsdocs/api/remplacerpdf")


//Serveur de pr� production
//#define ADR_HTTP_RECONCILIER      QString("https://pp.4axes.net/api/reconcillier")
//#define ADR_HTTP_GETNNI           QString("https://pp.4axes.net/api/getnni")
//#define ADR_HTTP_ENVOYER_SEC      QString("https://pp.4axes.net/api/envoisec")
//#define ADR_HTTP_DOWNLOAD         QString("https://pp.4axes.net/api/download")
//#define ADR_HTTP_RECHERCHE        QString("https://pp.4axes.net/api/recherche")
//#define ADR_HTTP_GETMESSAGE       QString("https://pp.4axes.net/api/getmessage")
//#define ADR_HTTP_DOWNLOAD_EDS     QString("https://pp.4axes.net/api/geteds")
//#define ADR_HTTP_ENVOYER_SWITCH   QString("https://pp.4axes.net/api/remplacerpdf")


//Serveur de production
#define ADR_HTTP_RECONCILIER      QString("https://4axes.net/api/reconcillier")
#define ADR_HTTP_GETNNI           QString("https://4axes.net/api/getnni")
#define ADR_HTTP_ENVOYER_SEC      QString("https://4axes.net/api/envoisec")
#define ADR_HTTP_DOWNLOAD         QString("https://4axes.net/api/download")
#define ADR_HTTP_RECHERCHE        QString("https://4axes.net/api/recherche")
#define ADR_HTTP_GETMESSAGE       QString("https://4axes.net/api/getmessage")
#define ADR_HTTP_DOWNLOAD_EDS     QString("https://4axes.net/api/geteds")
#define ADR_HTTP_ENVOYER_SWITCH   QString("https://4axes.net/api/remplacerpdf")








//----------------------------------------------------------------------------------------------------------------------
/*
  Constructeur
  */
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow){

    ui->setupUi(this);
    this->setFixedSize (806,709);
    this->setWindowTitle ("API - R�conciliation manuelle");

    QStringList listeArguments = QCoreApplication::arguments ();

    QDateTime dateTime;

    //Archivage fichier log de la veille
    QFileInfo infoFichierLog( QCoreApplication::applicationDirPath () % "/" + NOM_FICHIER_LOG );
    if( infoFichierLog.lastModified ().toString ("yyyyMMdd") < dateTime.currentDateTime ().toString ("yyyyMMdd") ){

        QFile fichier;
        fichier.rename ( QCoreApplication::applicationDirPath () % "/" % NOM_FICHIER_LOG,
                         QCoreApplication::applicationDirPath ()
                         % "/" % NOM_FICHIER_LOG.append ( infoFichierLog.lastModified ().toString ("yyyyMMdd") ) );
    }
    //Initialisation du flag d'improtation
    importationEDS = false;

    //Initialisation de la progressBar
    ui->progressBar->setValue (0);

    ui->checkBox_selectionnerTout->setVisible ( false );

    //Initialisation du chrono pour le temps d'envoi de la recherche
    time = new QTime();

    //Initialisation du chrono pour le temps d'envoi du fichier
    chrono = new QTime();

    siVmImporter = false;

    //Initialisation des boutons pour t�l�charger la DPEC/RPEC
    ui->btn_telechargerDPEC->setEnabled (false);
    ui->btn_telechargerRPEC->setEnabled (false);

    //On r�cup�re le nom de la machine
    hostname = QHostInfo::localHostName();

    //Initialisation de la structure de contexte
    typeContexte.recherche      = "recherche";
    typeContexte.simple         = "reconciliation_simple";
    typeContexte.multiple       = "reconciliation_multiple";
    typeContexte.SEC            = "reconciliation_SEC";
    typeContexte.statutRPEC     = "statutRPEC";
    typeContexte.switchRPEC     = "switch";

    //Initialisation de la structure des statuts DPEC
    StatutDPEC.acquitte             = "acquitte";
    StatutDPEC.annule               = "annule";
    StatutDPEC.remis                = "remis";
    StatutDPEC.purge                = "purge";

    //Iniatialisation structure PoperFax
    structPopper.good               = "GoodNIN";
    structPopper.unknown            = "UnknownNIN";

    //Initalisation de la structure listant les erreurs
    structErreur.insertEchoue       = "Insert echoue";
    structErreur.reponseEmise       = "Reponse deja emise";
    structErreur.mauvaisRNM         = "Impossible de recuperer les entites";

    //Positionne l'application � droite
    QDesktopWidget *bureau = QApplication::desktop();
    int x_bureau = bureau->width();
    int y_bureau = bureau->height();
    int x = x_bureau / 3 + width()/7;
    int y = y_bureau / 2 - height()/1.8;
    move( QPoint(x, y) );

    ui->lineEdit_mdpSWITCH->setVisible (false);
    //Masque les caract�res lors de la saisie du mot de passe
    ui->lineEdit_mdpSWITCH->setEchoMode ( QLineEdit::Password );


    if( listeArguments.length () == 2 ){

        //On r�cup�re le path de la mutuelle � traiter
        pathMutuelleEnCours = listeArguments.at (1);

        //On affiche les fichiers PDF du dossier
        listerFichier(pathMutuelleEnCours, false);
        listerTMP(pathMutuelleEnCours);
        listerVM();
    }
}















//----------------------------------------------------------------------------------------------------------------------
/*
  Destructeur
  */
MainWindow::~MainWindow(){


    delete ui;
}

















//----------------------------------------------------------------------------------------------------------------------
/*
  Lancement de la requ�te avec comme param�tre le nom du fichier correspondant au NNI � chercher sur 4AXES.NET
  */
void MainWindow::lancerRequete (QFileInfo informationFichier){

    //On r�cup�re seulement le nom du fichier sans l'extension
    nomFichier = informationFichier.fileName ();
    QFileInfo infoFichier( pathMutuelleEnCours % "/" % nomFichier);

    if( !nomFichier.isEmpty () ){

        //RAZ de la progressBar
        ui->progressBar->setValue ( 0 );

        ecrireLog (NOM_FICHIER_LOG, "-----------------------------------------------------------------");
        ecrireLog (NOM_FICHIER_LOG, "Path fichier : " % pathFichier);
        ecrireLog (NOM_FICHIER_LOG, "Mutuelle en cours : " % mutuelleEnCours);
        ecrireLog (NOM_FICHIER_LOG, "Nom fichier : " % nomFichier);
        ecrireLog (NOM_FICHIER_LOG, "Taille du fichier : " % QString::number ( infoFichier.size () / 1024 ) % " Ko" );
        ecrireLog (NOM_FICHIER_LOG, "Lancement API sur " % ADR_HTTP_GETNNI );
        ecrireLog (NOM_FICHIER_LOG, "Traitement du NNI : " % informationFichier.baseName () );

        ui->lineEdit_nomFichier->setText (nomFichier);

        QUrl parametre;
        parametre.addQueryItem ( "nni", informationFichier.baseName () );
        parametre.addQueryItem ( "type", "etendu" );

        //Pr�paration de la requ�te
        QNetworkAccessManager *m_network;
        m_network = new QNetworkAccessManager(this);

        QNetworkRequest request;
        request.setHeader( QNetworkRequest::ContentTypeHeader, QVariant("application/x-www-form-urlencoded") );
        request.setRawHeader (HTTP_DOMAINE_4AXES.toAscii (), HTTP_DOMAINE_4AXES_VALEUR.toAscii ());
        request.setUrl( QUrl ( ADR_HTTP_GETNNI ) );

        ui->statusBar->showMessage ("Requ�te sur " % ADR_HTTP_GETNNI);

        time->start ();

        QNetworkReply *reply = m_network->post( request, parametre.encodedQuery () );

        //Connexion r�ponse
        connect(m_network,
                SIGNAL( finished(QNetworkReply *) ),
                this,
                SLOT( retour_recherche(QNetworkReply *) ) );

        connect(reply,
                SIGNAL(error(QNetworkReply::NetworkError)),
                this,
                SLOT(messageErreur(QNetworkReply::NetworkError) ) );

        connect(reply,
                SIGNAL(downloadProgress(qint64,qint64) ),
                this,
                SLOT(progressionTelechargement(qint64, qint64) ) );
    }
    else
        debugger ( "Param�tre manquant","Le NNI est manquant - Impossible de lancer la requ�te (Phase 1)" );

}






















//----------------------------------------------------------------------------------------------------------------------
/*
  Gestion des erreurs lors de la requ�te POST
  */
void MainWindow::messageErreur(QNetworkReply::NetworkError){

    QNetworkReply *r = qobject_cast<QNetworkReply*>(sender());
    ecrireLog (NOM_FICHIER_LOG,"ERREUR : Phase 1 -> requ�te : " % r->errorString () );
    debuggerInfo ("Erreur requ�te","Erreur requ�te : " % r->errorString());
}





















//----------------------------------------------------------------------------------------------------------------------
/*
  SLOT : recup�re le retour de la requ�te effectuant la recherche du NNI
  sur la plateforme

  Param�tre d'entr�e :
            > retour de la requ�te
  */
void MainWindow::retour_recherche(QNetworkReply *reply){

    if ( reply->error() > 0 ){

        ui->zoneTexte->setPlainText ( reply->errorString() );
        debugger ( "Erreur retour WebService ","Erreur lors de la recherche / cf. Zone erreur" );
        ecrireLog (NOM_FICHIER_LOG, ERREUR_RECHERCHE % " : " % reply->errorString() );
    }
    else {
        QByteArray data = reply->readAll();

        if ( data.length () > 0 ){

            ui->zoneTexte->setPlainText ("R�ponse OK");

            ecrireLog ("reconciliation.log","Taille de la r�ponse : " % QString::number ( data.size () ) % " octets " );

            parserReponse ( data, typeContexte.recherche );
        }
    }
}



































//----------------------------------------------------------------------------------------------------------------------
/*
  Parse la r�ponse du serveur au format XML

  Param�tre d'entr�e :
            > QString reponse => r�ponse du serveur
            > QString contexte :
                - "recherche" -> correspond � la r�ponse de la premi�re requ�te (recherche du NNI sur la BDD)
                - "reconciliation_simple" -> correspond � la r�ponse du serveur une fois le fichier envoy�
                - "reconciliation_multiple" -> correspondant � la r�ponse du serveur apr�s avoir envoy� plusieurs fichiers
                - "reconciliation_sec" -> correspondant � la r�ponse du serveur apr�s avoir envoy� un fichier en SEC
                - "SWITCH" ->

    N�cessite : R�ponse serveur non vide + Contexte de r�conciliation (cf. Structure contexte)
  */
void MainWindow::parserReponse (QString reponse, QString contexte){

    if( !reponse.isEmpty () ){

        reponse.replace ("&"," et ");

        int tempsEnvoiFichier = time->elapsed ();

        ecrireLog (NOM_FICHIER_LOG,"Temps de traitement dans le contexte "
                   % contexte % " : " % QString::number ( tempsEnvoiFichier )% " ms" );

        ui->label_tempsRequete->setText ("Temps requ�te : " % QString::number ( tempsEnvoiFichier ) % " ms" );

        if( contexte == typeContexte.recherche ){

            Dial_bdd *bdd_stats = new Dial_bdd(QCoreApplication::applicationDirPath () % "/Base_stats");
            bdd_stats->enregistrerStats ( QDate::currentDate ().toString ("yyyyMMdd"), "recherche" ,
                                          "manuelle", QString::number ( tempsEnvoiFichier ) );

            listeDPEC.clear ();

            QString nbResultat = 0;
            QString erreur;

            QString idMessage;
            QString idMessageRPEC;
            QString emetteur;
            QString destinataire;
            QString statutDPEC;
            QString idClientD;
            QString idClientE;
            QString numDossier;
            QString dateEntree;
            QString nomPatient;
            QString prenomPatient;

            bool demande = false;
            bool info = false;

            QXmlStreamReader xmlReader(reponse);

            while( !xmlReader.atEnd () && !xmlReader.hasError ()){

                xmlReader.readNext ();

                //Si c'est une valise de d�but et la balise est <Reponse>
                if( xmlReader.isStartElement () && xmlReader.name ().toString () == "Reponse" ){

                    //Si l'attribut "resultat" est � 1 (OK)
                    if (xmlReader.attributes ().value ("resultat") == "1")
                        demande = true;
                }
                else if( xmlReader.isStartElement () && xmlReader.name ().toString ().contains ("infos") && !info ){

                    info = true;
                }
                else if ( xmlReader.isEndElement () && xmlReader.name ().toString ().contains ("infos") && info ){

                    info = false;

                    DPEC maStrucutre;
                    maStrucutre.idMessageDPEC   = idMessage;
                    maStrucutre.idMessageRPEC   = idMessageRPEC;
                    maStrucutre.statutDPEC      = statutDPEC;
                    maStrucutre.statutRPEC      = "N/C";
                    maStrucutre.emetteur        = emetteur;
                    maStrucutre.idClientE       = idClientE;
                    maStrucutre.destinataire    = destinataire;
                    maStrucutre.idClientD       = idClientD;
                    maStrucutre.dateEntree      = dateEntree;
                    maStrucutre.nomPatient      = nomPatient;
                    maStrucutre.prenomPatient   = prenomPatient;
                    maStrucutre.numDossier      = numDossier;

                    listeDPEC.push_back (maStrucutre);

                }
                else if(xmlReader.isStartElement () && xmlReader.name ().toString () == "nbResultat" )
                    nbResultat = xmlReader.readElementText ();

                else if( xmlReader.isStartElement () && demande ){

                        if( xmlReader.name ().toString () == "idMessageDPEC" )
                            idMessage = xmlReader.readElementText ();

                        else if( xmlReader.name ().toString () == "idMessageRPEC")
                            idMessageRPEC = xmlReader.readElementText ();

                        else if( xmlReader.name ().toString () == "statutDPEC" )
                            statutDPEC = xmlReader.readElementText ();

                        else if( xmlReader.name ().toString () == "emetteur" )
                            emetteur = xmlReader.readElementText ();

                        else if( xmlReader.name ().toString () == "destinataire" )
                            destinataire =  xmlReader.readElementText ();

                        else if( xmlReader.name ().toString () == "dateEntree" )
                               dateEntree =  xmlReader.readElementText ();

                        else if( xmlReader.name ().toString () == "idClientD" ){

                            idClientD =  xmlReader.readElementText ();
                            idMutuellePlateforme = idClientD;
                        }

                        else if( xmlReader.name ().toString () == "idClientE" )
                            idClientE =  xmlReader.readElementText ();

                        else if( xmlReader.name ().toString () == "nomPatient" )
                            nomPatient =  xmlReader.readElementText ();

                        else if( xmlReader.name ().toString () == "prenomPatient" )
                            prenomPatient =  xmlReader.readElementText ();

                        else if( xmlReader.name ().toString () == "numDossier" )
                            numDossier =  xmlReader.readElementText ();
                    }
                    else if( !demande ){

                        if( xmlReader.name ().toString () == "erreur" )
                            erreur = xmlReader.readElementText ();
                    }
                }

            //Si r�ponse n�gative
            if( !demande ){

                ui->zoneTexte->setText (erreur);
                ecrireLog ( NOM_FICHIER_LOG, erreur);
                nbResultat = "Aucun r�sultat";
            }

            if( nbResultat.toInt () != listeDPEC.length () )
                debuggerInfo ("Nombre �l�ment",
                              "Le nombre de r�sultat retourn� et diff�rent du nombre de DPEC affich�e"
                              "-- Veuillez v�rifier si des carac�res sp�ciaux sont pr�sents dans la r�ponse de 4AXES.NET");

            ecrireLog ("reconciliation.log","Nombre de r�sultat retourn� par le serveur : " % nbResultat);
            remplirTableau(listeDPEC);
        }
        else if( contexte.contains( "reconciliation" ) ){

            int nbIdChronoErreur = 0;

            if( reponse.contains ("error") ){

                ui->zoneTexte->setPlainText (reponse);
                debugger ("Erreur r�conciliation","Erreur lors de la r�conciliation / cf. Zone erreur");
                ecrireLog (NOM_FICHIER_LOG,"Erreur phase 2 : " % reponse);

            }
            else{

                QString erreur;
                QString details;
                QString idChrono;

                //QString -> flux XML
                QXmlStreamReader xmlReader(reponse);

                bool reconcilier = false;
                bool nonReoncilier = true;
                bool reconciliationEnCours = false;

                if( contexte == typeContexte.multiple ){

                    /*
                      On �pure pour obtenir une seule balise de d�but et une seule balise de fin
                      */
                    reponse.replace ("<?xml version=\"1.0\" encoding=\"UTF-8\"?>","");
                    reponse.prepend ("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");

                    reponse.replace ("</Reponse>","");
                    reponse.append ("</Reponse>");
                }

                //Tant qu'on est pas arriv� � la fin
                while( !xmlReader.atEnd () && !xmlReader.hasError () ){

                    //Lecture balise
                    xmlReader.readNext ();

                    //Si resultat est OK
                    if( xmlReader.isStartElement () && xmlReader.name ().toString () == "Reponse" &&
                            xmlReader.attributes ().value ("resultat") == "1"){

                        reconcilier = true;
                        reconciliationEnCours = true;
                    }

                    //Si resultat est NOK
                    if( xmlReader.isStartElement () && xmlReader.name ().toString () == "Reponse" &&
                            xmlReader.attributes ().value ("resultat") == "0"){

                        nonReoncilier = false;
                        reconciliationEnCours = false;
                    }
                    else if( xmlReader.isStartElement () && reconcilier && reconciliationEnCours ){

                        while( xmlReader.isStartElement () ){

                            if( xmlReader.name ().toString () == "idChrono" )
                                ecrireLog (NOM_FICHIER_LOG,"ID_CHRONO : " % xmlReader.readElementText () );

                            else if( xmlReader.name ().toString () == "statutMessage" )
                                ecrireLog (NOM_FICHIER_LOG,"Statut RPEC : " % xmlReader.readElementText () );

                            xmlReader.readNext ();
                        }
                    }
                    else{

                        while( xmlReader.isStartElement () ){

                            if( xmlReader.name ().toString () == "idChrono" ){

                                nbIdChronoErreur++;

                                idChrono = xmlReader.readElementText ();
                                ecrireLog (NOM_FICHIER_LOG,"ID_CHRONO erreur : " % idChrono );
                            }
                            else if( xmlReader.name ().toString () == "erreur" ){

                                erreur = xmlReader.readElementText ();
                                ecrireLog (NOM_FICHIER_LOG,"ERREUR : " % erreur) ;
                            }
                            else if( xmlReader.name ().toString () == "details" ){

                                details = xmlReader.readElementText ();
                                ecrireLog (NOM_FICHIER_LOG,"Detail : " % details);

                                system("C:\\Postie\\postie.exe -from:VM@4axes.fr -to:vincent.parramon@4axes.fr -host:mail.4axes.fr -s:\"Erreur API \"  -msg:\"Erreur : VM -> "
                                       + hostname.toLocal8Bit ()
                                       + " - RNM -> " + rnm_courant.toLocal8Bit ()
                                       + " - Details : \"" + details.toLocal8Bit () + "\"");
                            }
                            xmlReader.readNext ();
                        }
                    }
                }
                if( reconcilier && nonReoncilier ){

                    if( ui->radioButton_erreurDest->isChecked () ){
                        ecrireLog(NOM_FICHIER_LOG,"Nouvelle erreur destinataire");
                        contexte = "Erreur destinataire";
                    }


                    if( contexte == typeContexte.SEC )
                        ecrireLog(NOM_FICHIER_LOG, "Message envoye en SEC");
                    else
                        ecrireLog (NOM_FICHIER_LOG, "Message reconcilie");

                    //Mise � jour base de donn�es pour statistique
                    Dial_bdd *bdd = new Dial_bdd(config->getPathINI () % "/" % hostname);
                    bdd->majDonnee ( config->getUserCourant (), "NBMANU", 1);

                    //TODO
                    Dial_bdd *bdd_stats = new Dial_bdd(QCoreApplication::applicationDirPath () % "/Base_stats");
                    bdd_stats->enregistrerStats ( QDate::currentDate ().toString ("yyyyMMdd"), "envoiFichier" ,
                                                  "manuelle",QString::number ( tempsEnvoiFichier ) );

                    bdd_stats->enregistrerReconciliation ( nomFichier,
                                                           ui->lineEdit_IDMsg->text (),
                                                           config->getUserCourant (),
                                                           hostname,
                                                           contexte,
                                                           ui->lineEdit_nomPatient->text (),
                                                           ui->lineEdit_prenomPatient->text (),
                                                           ui->lineEdit_numDossier->text (),
                                                           ui->lineEdit_dateEntree->text (),
                                                           ui->lineEdit_emetteur_DPEC->text (),
                                                           ui->lineEdit_dest_DPEC->text (),
                                                           mutuelleEnCours);


                    //Si l'utilisateur ne souhaite pas que l'appli d�place le fichier apr�s traitement
                    if( !ui->checkBox_nonDeplacement->isChecked () ){

                        fermerFichierPDF();

                        if( deplacerFichier ("OK_MANUEL", true) ){

                            debuggerInfo ("Confirmation r�conciliation","R�conciliation effectu�e");
                            ui->zoneTexte->setPlainText ("R�conciliation effectu�e");
                            delete ui->listWidget_fichierPDF->currentItem ();

                            viderWidget ();

                            //Une fois qu'il n'y a plus de fichier dans le dossier
                            if( ui->listWidget_fichierPDF->count () == 0 )
                                QCoreApplication::quit ();
                        }
                        else{

                            debuggerInfo ("Confirmation r�conciliation","R�conciliation effectu�e "
                                          "- Fichier toujours pr�sent");
                            ui->zoneTexte->setPlainText ("R�conciliation effectu�e mais fichier non d�plac� "
                                                         "- CHECKBOX coch�e");
                        }
                    }
                    else
                        debuggerInfo ("Confirmation r�conciliation","R�conciliation effectu�e mais non d�plac�");
                }
                else{

                    ecrireLog (NOM_FICHIER_LOG, "Message non reconcilie - Nombre de DPEC non reconciliee : "
                               % QString::number ( nbIdChronoErreur ) );

                    //Si erreur
                    if( erreur == structErreur.insertEchoue || erreur == structErreur.reponseEmise )
                        ui->zoneTexte->setPlainText ( erreur %  " -> " % ERREUR_RECONCILIATION_01 );

                    else if ( erreur == structErreur.mauvaisRNM )
                        ui->zoneTexte->setPlainText ( erreur % " -> " % ERREUR_RECONCILIATION_02 );

                    else
                        ui->zoneTexte->setPlainText ( erreur );

                    debuggerInfo ("Erreur retour","Erreur lors de l'envoi du fichier / cf. Zone erreur");
                }

//                viderWidget ();
            }
        }
        else if( contexte == typeContexte.statutRPEC ){

            bool resultat = false;

            QString statutRPEC;
            QString erreur;

            QXmlStreamReader xmlReader(reponse);

            while( !xmlReader.atEnd () && !xmlReader.hasError () ){

                xmlReader.readNext ();

                //Si resultat est OK
                if( xmlReader.isStartElement () && xmlReader.name ().toString () == "Reponse" &&
                        xmlReader.attributes ().value ("resultat") == "1")
                    resultat = true;

                 else if( xmlReader.isStartElement () && resultat ){

                    if( xmlReader.name ().toString () == "statut" )
                        statutRPEC = xmlReader.readElementText ();
                }
                else{

                    if( xmlReader.name ().toString () == "erreur" ){

                        erreur = xmlReader.readElementText ();
                        debuggerInfo ("Erreur statut RPEC","Impossible de r�cup�rer le statut de la RPEC | Erreur : " % erreur);
                        ecrireLog (NOM_FICHIER_LOG,"Erreur recuperation statut RPEC : " % erreur);
                    }
                }
            }
            if( resultat ){

                for(int i = 0 ; i < listeDPEC.length () ; i++){

                    if( idMsgSelect == listeDPEC[i].idMessageDPEC ){

                        //On met � jour la structure
                        listeDPEC[i].statutRPEC = statutRPEC;

                        ecrireLog (NOM_FICHIER_LOG,"Mise � jour statut RPEC. DPEC -> "
                                   % idMsgSelect % " / Statut RPEC -> " % statutRPEC );

                        //On alimente l'IHM de suite
                        ui->lineEdit_statutRPEC->setText ( statutRPEC );
                    }
                }
            }
        }
        else if( contexte == typeContexte.switchRPEC ){

            bool switcher = false;
            QString retourSwitch;
            QString erreur;

            QXmlStreamReader xmlReader(reponse);

            while( !xmlReader.atEnd () && !xmlReader.hasError () ){

                xmlReader.readNext ();

                //Si resultat est OK
                if( xmlReader.isStartElement () && xmlReader.name ().toString () == "Reponse" &&
                        xmlReader.attributes ().value ("resultat") == "1")
                    switcher = true;

                //Si le switch a bien �t� effectu� ET que nous nous trouvons sur la balise de d�but <sucess>
                else if( xmlReader.isStartElement () && switcher && xmlReader.name ().toString () == "sucess")
                       retourSwitch = xmlReader.readElementText ();

               else if( xmlReader.name ().toString () == "erreur" && !switcher )
                   erreur = xmlReader.readElementText ();
            }

            if( switcher ){

                ui->zoneTexte->setPlainText ("SWITCH correctement effecut�");
                ecrireLog ( NOM_FICHIER_LOG, "SWITCH correctement effecut� sur l'idMessage : "
                            % getIdMsgRPEC ( idMsgSelect, listeDPEC ) );

                debuggerInfo ("Confirmation SWITCH", "Le SWITCH a bien �t� effectu�");


                deplacerFichier ("OK_MANUEL", true);
                delete ui->listWidget_fichierPDF->currentItem ();

                Dial_bdd *bdd = new Dial_bdd(config->getPathINI () % "/" % hostname);
                bdd->majDonnee ( config->getUserCourant (), "NBMANU", 1);

                Dial_bdd *bdd_stats = new Dial_bdd(QCoreApplication::applicationDirPath () % "/Base_stats");

                bdd_stats->enregistrerReconciliation ( nomFichier,
                                                       ui->lineEdit_IDMsg->text (),
                                                       config->getUserCourant (),
                                                       hostname,
                                                       "SWITCH",
                                                       ui->lineEdit_nomPatient->text (),
                                                       ui->lineEdit_prenomPatient->text (),
                                                       ui->lineEdit_numDossier->text (),
                                                       ui->lineEdit_dateEntree->text (),
                                                       ui->lineEdit_emetteur_DPEC->text (),
                                                       ui->lineEdit_dest_DPEC->text (),
                                                       mutuelleEnCours );

                viderWidget ();

                //Une fois qu'il n'y a plus de fichier dans le dossier
                if( ui->listWidget_fichierPDF->count () == 0 )
                    QCoreApplication::quit ();

            }
            else{

                debuggerInfo ("Erreur retour","Erreur lors de l'envoi du fichier / cf. Zone erreur");
                ui->zoneTexte->setPlainText ("Erreur SWITCH RPEC : " % erreur);
                ecrireLog ( NOM_FICHIER_LOG, "Erreur SWITCH RPEC : " % erreur);
            }
        }
    }
    else{

        ecrireLog (NOM_FICHIER_LOG,"Reponse du contexte " % contexte % " vide - SSL ?");
        ui->zoneTexte->setText ("Reponse du contexte " % contexte % " vide - SSL ?");
    }
}


























//----------------------------------------------------------------------------------------------------------------------
/*
  Remplit la QListWigdet avec la liste de structure correspondant au r�sultat
  Chaque item est accompagn� d'un QRadioButton
  */
void MainWindow::remplirTableau(QList<DPEC> listeStruct){

    //R�initialisation de la liste de widget
    ui->listWidget_DPEC->clear ();

    //Affichage du nombre de r�sultat apr�s requ�te
    ui->label_nbDPEC->setText ("Nombre de DPEC : " % QString::number ( listeStruct.length () ) );

    QString idMessage;

    //Affichage des �l�ments
    for( int i = 0 ; i < listeStruct.length () ; i++ ){

        ecrireLog (NOM_FICHIER_LOG, "idMessageDPEC : " % listeStruct[i].idMessageDPEC
                   % " / Statut : " % listeStruct[i].statutDPEC
                   % " / Emetteur : " % listeStruct[i].emetteur
                   % " / Destinataire : " % listeStruct[i].destinataire
                   % " / Date entree : " % listeStruct[i].dateEntree
                   % " / Nom patient : " % listeStruct[i].nomPatient
                   % " / Numero dossier : " % listeStruct[i].numDossier
                   );

        idMessage = listeStruct[i].idMessageDPEC;

        QListWidgetItem *item = new QListWidgetItem(idMessage);
        ui->listWidget_DPEC->addItem (item);
    }
}



























//----------------------------------------------------------------------------------------------------------------------
/*
  Remplit la QListWigdet avec la liste de structure correspondant au r�sultat
  Chaque item est accompagn� d'un QCheckBox
  */

void MainWindow::remplirTableauCheckBox (QList<DPEC> listeStruct, bool checked){

    ui->listWidget_DPEC->clear ();

    QString idMessage;

    for( int i = 0 ; i < listeStruct.length () ; i++ ){

        idMessage = listeStruct[i].idMessageDPEC;

        QListWidgetItem *item = new QListWidgetItem(idMessage);

        ui->listWidget_DPEC->addItem (item);

        if( checked )
            ui->listWidget_DPEC->item (i)->setCheckState ( Qt::Checked );
        else
            ui->listWidget_DPEC->item (i)->setCheckState ( Qt::Unchecked );
    }
}




























//----------------------------------------------------------------------------------------------------------------------
/*
  Enregistre chaque IdMessage des DPEC cocher dans le cadre d'une multi-r�conciliation
  */
void MainWindow::compterDPECSelectionner (QStringList &listeDPECARecon){

    for (int i = 0; i < ui->listWidget_DPEC->count(); ++i) {

        if( ui->listWidget_DPEC->item(i)->checkState () == Qt::Checked )
            listeDPECARecon.push_back ( ui->listWidget_DPEC->item (i)->text () );
    }
}









//----------------------------------------------------------------------------------------------------------------------
/*
  Enregistre chaque nom de fichier cocher dans le cadre d'une multi-s�lection
  */
void MainWindow::compterFichierSelectionner (QStringList &listeFichierSelectionner){

    for (int i = 0; i < ui->listWidget_fichierPDF->count(); i++) {

        if( ui->listWidget_fichierPDF->item(i)->checkState () == Qt::Checked )
            listeFichierSelectionner.push_back ( ui->listWidget_fichierPDF->item (i)->text () );
    }
}






















//----------------------------------------------------------------------------------------------------------------------
/*
  Ex�cution de la r�conciliation/action
  */
void MainWindow::on_btn_executer_clicked(){

    if( !ui->radioButton_reconcilier->isChecked () && !ui->radioButton_erreurDest->isChecked ()
            && !ui->radioButton_envoyerSEC->isChecked () && !ui->radioButton_ArchivageNR->isChecked ()
            && !ui->radioButton_switch->isChecked () && !ui->radioButton_ArchivageNT->isChecked () )
       debugger ("S�lection action","Veuillez s�lectionner une action !");

    else{

        if( config->getUserCourant () == "NULL" || config->getCrashSuperViseur() )
             debuggerInfo ("Connexion profil","Veuillez vous connecter � votre compte SuperViseur pour effectuer une action");
        else{

            if( !pathFichier.isEmpty () ){

                chrono->start ();

                QStringList listeDPECARecon;
                listeDPECARecon.clear ();

                //Archivage NR
                if( ui->radioButton_ArchivageNR->isChecked () ){

                    //Log
                    ecrireLog ( NOM_FICHIER_LOG, "Action -> Archivage NR");

                    Dial_bdd *bdd = new Dial_bdd(config->getPathINI () % "/" % hostname);
                    //Met � jour les statistiques
                    bdd->majDonnee ( config->getUserCourant (), "NBNR", 1);

                    Dial_bdd *bdd_stats = new Dial_bdd(QCoreApplication::applicationDirPath () % "/Base_stats");

                    bdd_stats->enregistrerReconciliation ( nomFichier,
                                                           ui->lineEdit_IDMsg->text (),
                                                           config->getUserCourant (),
                                                           hostname,
                                                           "NR",
                                                           ui->lineEdit_nomPatient->text (),
                                                           ui->lineEdit_prenomPatient->text (),
                                                           ui->lineEdit_numDossier->text (),
                                                           ui->lineEdit_dateEntree->text (),
                                                           ui->lineEdit_emetteur_DPEC->text (),
                                                           ui->lineEdit_dest_DPEC->text (),
                                                           mutuelleEnCours );

                    //Si non d�placement
                    if( !ui->checkBox_nonDeplacement->isChecked () ){

                        fermerFichierPDF();

                        Sleep(100);

                        //D�placement fichier dans NR
                        if ( deplacerFichier("NR", true) ){

                            debuggerInfo ("Archivage fichier","Archiv� dans NR");
                            delete ui->listWidget_fichierPDF->currentItem ();

                            viderWidget ();

                            //Une fois qu'il n'y a plus de fichier dans le dossier
                            if( ui->listWidget_fichierPDF->count () == 0 )
                                QCoreApplication::quit ();
                        }
                        else
                            debugger ("Archivage fichier","Fichier non d�plac�");
                    }
                    else
                        debugger ("Archivage fichier","Fichier non archiv� / Non d�plac�");

                }
                else if ( ui->radioButton_ArchivageNT->isChecked () ){          //Archivage Non Traitable

                    //Log
                    ecrireLog ( NOM_FICHIER_LOG, "Action -> Archivage NT");

                    Dialog_nonTraitable *dialog_NT = new Dialog_nonTraitable();
                    connect (dialog_NT, SIGNAL(on_motifValider(QString,bool)), this, SLOT(archiverNT(QString,bool)));
                    dialog_NT->show ();
                }

                //R�conciliation simple sans erreur destinataire
                else if( ui->radioButton_reconcilier->isChecked () ){

                    ecrireLog ( NOM_FICHIER_LOG, "Action -> R�concilier");

                    fermerFichierPDF();
                    Sleep(100);

                    //Multi r�conciliation simple
                    if( ui->checkBox_multiReconciliation->isChecked () ){

                        //Compter le nombre de DPEC coch�e
                        compterDPECSelectionner(listeDPECARecon);

                        if( ! listeDPECARecon.isEmpty () ){

                            if( ! verifierStatut( listeDPECARecon, listeDPEC ) ){

                                rnm_courant = "1";
                                envoyerFichierMultiple ( listeDPECARecon, rnm_courant );
                            }
                            else
                                debugger ("Multi-r�conciliation incorrect","Impossible de r�concilier une DPEC acquitt�e / Purg�e / Annul�e !");
                        }
                        else
                            debugger ("S�lection DPEC","Veuillez s�lectionner une ou plusieurs DPEC � r�concilier !");
                    }
                    else{

                        if( !idMsgSelect.isEmpty () ){

                            rnm_courant = "1";
                            envoyerFichierSimple ( idMsgSelect, rnm_courant );
                        }
                        else
                            debugger ("S�lection DPEC","Veuillez s�lectionner une DPEC � r�concilier !");
                    }
                }
                //Erreur destinataire
                else if( ui->radioButton_erreurDest->isChecked () ){

                    ecrireLog ( NOM_FICHIER_LOG, "Action -> Erreur destinataire");

                    fermerFichierPDF();
                    Sleep(100);

                    //Multi
                    if( ui->checkBox_multiReconciliation->isChecked () ){

                        //Compter le nombre de DPEC coch�e
                        compterDPECSelectionner(listeDPECARecon);

                        if( ! listeDPECARecon.isEmpty () ){

                            if( !verifierStatut( listeDPECARecon, listeDPEC ) )
                                envoyerFichierMultiple ( listeDPECARecon, rnm_courant );
                            else
                                debugger ("Multi-r�conciliation incorrect","Impossible de r�concilier une DPEC acquitt�e !");
                        }
                    }
                    //Simple
                    else if( !ui->checkBox_multiReconciliation->isChecked () )
                        envoyerFichierSimple ( idMsgSelect, rnm_courant);
                }
                //Envoi en SEC
                else if( ui->radioButton_envoyerSEC->isChecked () ){

                    ecrireLog ( NOM_FICHIER_LOG, "Action -> Envoi en SEC");

                    fermerFichierPDF();
                    Sleep(100);

                    if( !ui->checkBox_multiReconciliation->isChecked () ){

                        if( !idEDSSelect.isEmpty () )
                            envoyerFichierEnSec ( idEDSSelect, rnm_courant );
                        else
                            debugger ("S�lection DPEC","Veuillez s�lectionner une DPEC � envoyer en SEC !");
                    }
                    else
                        debugger ("Multi-r�conciliation interdite","Veuillez d�cocher la mutlti-r�conciliation pour l'envoi en SEC");
                }
                else if( ui->radioButton_switch->isChecked () ){

                    ecrireLog ( NOM_FICHIER_LOG, "Action ->SWITCH");

                    fermerFichierPDF();
                    Sleep(100);

                    if( ui->lineEdit_mdpSWITCH->text ().isEmpty () )
                        debugger ("Mot de passe","Veuillez entrer un mot de passe");
                    else{

                        if( ui->lineEdit_mdpSWITCH->text () != MOT_DE_PASSE_SWITCH )
                            debugger ("Mot de passe","Mot de passe �rron�");
                        else if( !idMsgSelect.isEmpty () )
                                envoyerSwitch( getIdMsgRPEC ( idMsgSelect, listeDPEC ) );
                    }
                }
            }
            else
                debugger ("S�lection fichier","Veuillez s�lectionner un fichier");
        }
    }
}

































//----------------------------------------------------------------------------------------------------------------------
/*
  Ex�cution du la fonction recherche
  */
void MainWindow::on_btn_rechercher_clicked(){

    ecrireLog ( NOM_FICHIER_LOG, "Action -> Rechercher");

    //Si l'importation a �t� faite ET si le champ du nom EDS pour la recherche n'est pas vide
    if( importationEDS && !ui->lineEdit_nomEDS->text ().isEmpty () ){

        ui->listWidget_DPEC->clear ();

        for( int i = 0 ; i < listeEDS.length () ; i++ ){

            if( listeEDS.at (i).contains ( ui->lineEdit_nomEDS->text ().toUpper () ) ){

                QListWidgetItem *item = new QListWidgetItem( listeEDS.at (i).mid ( 0, listeEDS.at (i).indexOf (";") ) );
                ui->listWidget_DPEC->addItem ( item );
            }
        }
    }
    else{

        if( ui->lineEdit_NNI->text ().isEmpty () && ui->lineEdit_nomPatient_rche->text().isEmpty ()
                && ui->lineEdit_numDossier_rche->text ().isEmpty () )
            debugger ("Champs recherche","Veuillez renseigner au moins les champs pour votre recherche !");
        else{

            ui->progressBar->setValue (0);

            ui->radioButton_envoyerSEC->setEnabled (true);
            ui->radioButton_ArchivageNR->setEnabled (true);
            ui->radioButton_erreurDest->setEnabled (true);
            ui->radioButton_reconcilier->setEnabled (true);

            ui->checkBox_multiReconciliation->setEnabled (true);
            ui->checkBox_nonDeplacement->setEnabled ( true );

            ui->lineEdit_IDMsg->clear ();
            ui->lineEdit_statutDPEC->clear ();
            ui->lineEdit_statutRPEC->clear ();
            ui->lineEdit_emetteur_DPEC->clear ();
            ui->lineEdit_dest_DPEC->clear ();
            ui->lineEdit_dateEntree->clear ();
            ui->lineEdit_nomPatient->clear ();
            ui->lineEdit_numDossier->clear ();

            importationEDS = false;

            QString NNI = ui->lineEdit_NNI->text ();
            QString nomPatient = ui->lineEdit_nomPatient_rche->text ();
            QString numDossier = ui->lineEdit_numDossier_rche->text ();
            QString nomMutuelle = ui->lineEdit_rnmMutuelle_rche->text ();
            QString eds = ui->lineEdit_nomEDS->text () ;

            ecrireLog (NOM_FICHIER_LOG,"Recherche de : ");
            ecrireLog (NOM_FICHIER_LOG, "NNI -> " % NNI
                       % " || nom -> " % nomPatient
                       % " || numero dossier -> " % numDossier
                       % " || nom mutuelle -> " % nomMutuelle
                       % " || EDS -> " % eds);

            //Envoi de la requ�te de recherche
            envoyerRecherche( NNI, nomPatient, numDossier, nomMutuelle, eds );
        }
    }
}



























//----------------------------------------------------------------------------------------------------------------------.
/*
  D�place un fichhier vers un dossier de destination pass� en param�tre
  */
bool MainWindow::deplacerFichier (QString dossierDestination, bool sousRepertoire){

    //Info du fichier a traiter
    QFileInfo fichierInfo(pathFichier);

    QDate date;
    QString nomDossier;

    if( sousRepertoire ){

         //M�morisation du dossier � la date du jour
         nomDossier = fichierInfo.absolutePath () % "/" % dossierDestination % "/"
                % date.currentDate ().toString ("yyyyMMdd");
    }
    else
        nomDossier = fichierInfo.absolutePath () % "/" % dossierDestination;

    QDir dossier;
    QDateTime timeFichier;
    QString nomFichierACopier = pdfSelect;

    //Si le dossier a la date du jour existe
    if( !dossier.exists ( nomDossier ) ){

        //Cr�ation du dossier
        if ( !dossier.mkpath ( nomDossier ) ){

            debuggerInfo ("Cr�ation dossier","Impossible de cr�er le dossier " % nomDossier);
            ecrireLog (NOM_FICHIER_LOG,"Cr�aton du dossier � la date du jour : " % nomDossier);
        }
    }

    QFile fichier;

    //V�rification si un fichier portant le m�me nom existe dans le dossier de destination
    if( fichier.exists ( nomDossier % "/" % nomFichierACopier ) ){

        //Ajout du dateTime en bout de fichier
        nomFichierACopier =  nomFichierACopier % "_" % timeFichier.currentDateTime ().toString ("hhmmss") % ".pdf" ;
        ecrireLog ( NOM_FICHIER_LOG,"Fichier deja existant / Renomme : " % nomFichierACopier );
    }

    //Si la copie est impossible
    if( !fichier.copy ( pathFichier, nomDossier % "/" % nomFichierACopier ) ){

        ecrireLog ( NOM_FICHIER_LOG,"Impossible de copier le fichier "
                    % nomFichierACopier % " -> " % fichier.errorString () );

        debugger("D�placer fichier","Impossible de d�palcer le fichier trait� dans le dossier " %
                 nomDossier % " | " % "Erreur : " % fichier.errorString () );

        ui->zoneTexte->setPlainText ("Impossible de d�placer le fichier trait� dans le dossier "
                                     % nomDossier % " | Erreur -> " % fichier.errorString () );

        return( false );
    }
    else{

        //Si la suppression apr�s copie est impossible
        if( !fichier.remove (pathFichier) ){

            ecrireLog (NOM_FICHIER_LOG,"Impossible de supprimer le fichier "
                       % nomFichierACopier % " -> " % fichier.errorString () );

            debugger("D�placer fichier","Impossible de supprimer le fichier " % nomFichierACopier
                     % " dans le dossier d'origine | Erreur : " % fichier.errorString () );

            return( false );
        }
        else{

            ecrireLog ( NOM_FICHIER_LOG,"Fichier deplace dans " % nomDossier );
            return( true );
        }
    }
}
































//----------------------------------------------------------------------------------------------------------------------
/*
  Envoi d'un fichier PDF (RPEC) vers un DPEC avec comme param�tre :
    -L'ID du message sur la plateforme
    -Path du fichier PDF � envoyer
    -URL de destination (4AXES.NET)

    N�cessite : ID message non vide

    S�curit� : header personnalis� dans la requ�te -> Key : domaine / valeur : 4axes

    Utilitaire pour envoyer un fichier : cUrl
        Programme : cUrl.exe
        -k : SSL
        -H (header) : "key:valeur"
        --form : envoi de donn�es
    */
void MainWindow::envoyerFichierSimple (QString idMessage, QString rnmEnCours){

    QFile fichier;
    if( !fichier.exists ( pathFichier ) ){

        ecrireLog (NOM_FICHIER_LOG,"Le fichier a envoy� n'existe pas ou a �t� d�plac� - R�conciliation annul�e");
        ecrireLog (NOM_FICHIER_LOG,"-----------------------------------");
        debugger ("Envoi fichier","Le fichier a envoy� n'existe pas ou a �t� d�plac� - R�conciliation annul�e");
        ui->zoneTexte->setText ("Le fichier " % pathFichier % " n'existe pas ou a �t� d�plac� - R�conciliation annul�e");
    }
    else{

        time->start ();

        ecrireLog (NOM_FICHIER_LOG,"Envoi du fichier sur " % ADR_HTTP_RECONCILIER);
        ecrireLog (NOM_FICHIER_LOG,"Reconciliation DPEC (" % idMessage % ")" );

        QProcess processus;

        ui->statusBar->showMessage ("Requ�te sur " % ADR_HTTP_RECONCILIER);

        //Lancer CURL avec idMesage / pathFichier / rnm mutuelle en cours / url 4axes.net
        QString cmd = QCoreApplication::applicationDirPath ()
                % "/CURL/curl.exe -k -H " % HTTP_DOMAINE_4AXES % ":" % HTTP_DOMAINE_4AXES_VALEUR
                % " --form id="% idMessage % " --form rnm=" % rnmEnCours
                % " --form \"fichier=@" % pathFichier % "\" " % ADR_HTTP_RECONCILIER;

        //Lancement de la commande avec cUrl.exe
        processus.start (cmd);

        //On lit la sortie pour connaitre le resultat de notre requ�te
        processus.setProcessChannelMode(QProcess::MergedChannels);
        processus.waitForFinished(-1);
        QString resultatPrompt = processus.readAll ();

        if( resultatPrompt.isEmpty () )
            ecrireLog (NOM_FICHIER_LOG,"Erreur processus : " % processus.errorString ());

        //Affichage de la r�ponse
        ui->zoneTexte->setText (resultatPrompt);

        //Analyse de la r�ponse
        parserReponse (resultatPrompt, typeContexte.simple );
    }
}





























//----------------------------------------------------------------------------------------------------------------------
/*
  Envoi d'un fichier PDF (RPEC) vers un ensemble de DPEC avec comme param�tre :
  -Une liste d'ID message
  -Path du fichier PDF � envoyer
  -URL de destination (4AXES.NET)

  N�cessite : liste d'ID message non vide

  S�curit� : header personnalis� dans la requ�te -> Key : domaine / valeur : 4axes

    Utilitaire pour envoyer un fichier : cUrl
        Programme : cUrl.exe
        -s : d�sactivation de la mesure de progression
        -k : SSL
        -H (header) : "key:valeur"
        --form : envoi de donn�es
  */
void MainWindow::envoyerFichierMultiple(QStringList listeIdMessage, QString rnmEnCours){

    QFile fichier;
    if( !fichier.exists ( pathFichier ) ){

        ecrireLog (NOM_FICHIER_LOG,"Le fichier a envoy� n'existe pas ou a �t� d�plac� - R�conciliation annul�e");
        ecrireLog (NOM_FICHIER_LOG,"-----------------------------------");
        debugger ("Envoi fichier","Le fichier a envoy� n'existe pas ou a �t� d�plac� - R�conciliation annul�e");
        ui->zoneTexte->setText ("Le fichier " % pathFichier % " n'existe pas ou a �t� d�plac� - R�conciliation annul�e");
    }
    else{

        time->start ();

        ecrireLog (NOM_FICHIER_LOG,"Envoi du/des fichiers sur " % ADR_HTTP_RECONCILIER);

        QProcess processus;
        QString cmd;
        QString resultatPrompt;
        QString reponseServeur;

        ui->statusBar->showMessage ("Requ�te sur " % ADR_HTTP_RECONCILIER);

        for (int i = 0; i < listeIdMessage.length () ; i++){

            ecrireLog (NOM_FICHIER_LOG,"Reconciliation DPEC (" % listeIdMessage.at (i) % ")" );

            cmd = QCoreApplication::applicationDirPath ()
                    % "/CURL/curl.exe -s -k -H " % HTTP_DOMAINE_4AXES % ":" % HTTP_DOMAINE_4AXES_VALEUR
                    % " --form id=" % listeIdMessage.at (i) %
                    " --form rnm=" % rnmEnCours % " --form \"fichier=@" % pathFichier % "\" " % ADR_HTTP_RECONCILIER;

            //Lancement de la commande avec cUrl.exe
            processus.start (cmd);

            //On lit la sortie pour connaitre le resultat de notre requ�te
            processus.setProcessChannelMode(QProcess::MergedChannels);
            processus.waitForFinished(-1);

            resultatPrompt = processus.readAll ();
            reponseServeur += resultatPrompt;

            if( resultatPrompt.isEmpty () )
                ecrireLog (NOM_FICHIER_LOG,"Erreur processus : " % processus.errorString ());

            resultatPrompt.clear ();

            // /!\ IMPORTANT : temporisation -> �vite d'envoyer les requ�tes au webService trop rapidement /!\
            Sleep(1000);
        }

        //Affichage de la r�ponse
        ui->zoneTexte->setText (reponseServeur);

        //Analyse de la r�ponse (concat�n�e)
        parserReponse (reponseServeur, typeContexte.multiple );
    }
}





























//----------------------------------------------------------------------------------------------------------------------
/*
  Envoi d'un fichier PDF (RPEC) vers un hopital :
    -L'ID du message sur la plateforme
    -Path du fichier PDF � envoyer
    -URL de destination (4AXES.NET)

    N�cessite : ID message non vide

    S�curit� : header personnalis� dans la requ�te -> Key : domaine / valeur : 4axes

    Utilitaire pour envoyer un fichier : cUrl
        Programme : cUrl.exe
        -k : SSL
        -H (header) : "key:valeur"
        --form : envoi de donn�es
    */
void MainWindow::envoyerFichierEnSec (QString idEDS, QString rnmEnCours){

    QFile fichier;
    if( !fichier.exists ( pathFichier ) ){

        ecrireLog (NOM_FICHIER_LOG,"Le fichier a envoy� n'existe pas ou a �t� d�plac� - R�conciliation annul�e");
        ecrireLog (NOM_FICHIER_LOG,"-----------------------------------");
        debugger ("Envoi fichier","Le fichier a envoy� n'existe pas ou a �t� d�plac� - R�conciliation annul�e");
        ui->zoneTexte->setText ("Le fichier " % pathFichier % " n'existe pas ou a �t� d�plac� - R�conciliation annul�e");
    }
    else{

        time->start ();

        ecrireLog (NOM_FICHIER_LOG,"Envoi du fichier en SEC sur " % ADR_HTTP_ENVOYER_SEC % " a l'entite " % idEDS);

        QProcess processus;

        ui->statusBar->showMessage ("Requ�te sur " % ADR_HTTP_RECONCILIER);

        //Lancer CURL avec idMesage / pathFichier / rnm mutuelle en cours / url 4axes.net
        QString cmd = QCoreApplication::applicationDirPath ()
                % "/CURL/curl.exe -k -H " % HTTP_DOMAINE_4AXES % ":" % HTTP_DOMAINE_4AXES_VALEUR
                % " --form emetteur="% rnmEnCours
                % " --form destinataire=" % idEDS
                % " --form \"fichier=@" % pathFichier % "\" " % ADR_HTTP_ENVOYER_SEC;

        //Lancement de la commande avec cUrl.exe
        processus.start (cmd);

        //On lit la sortie pour connaitre le resultat de notre requ�te
        processus.setProcessChannelMode(QProcess::MergedChannels);
        processus.waitForFinished(-1);
        QString resultatPrompt = processus.readAll ();

        if( resultatPrompt.isEmpty () )
            ecrireLog (NOM_FICHIER_LOG,"Erreur processus : " % processus.errorString ());

        //Affichage de la r�ponse
        ui->zoneTexte->setText (resultatPrompt);

        //Analyse de la r�ponse
        parserReponse (resultatPrompt, typeContexte.SEC );
    }
}
























//----------------------------------------------------------------------------------------------------------------------
/*
  Envoi une requ�te de recherche au webService

  S�curit� : header personnalis� dans la requ�te -> Key : domaine / valeur : 4axes
  */
void MainWindow::envoyerRecherche(QString NNI, QString nomPatient, QString numDossier, QString mutuelle, QString eds){

    time->start ();

    ecrireLog (NOM_FICHIER_LOG,"Recherche sur " % ADR_HTTP_RECHERCHE);

    QUrl parametre;
    parametre.addQueryItem ( "nni", NNI );
    parametre.addQueryItem ( "nomBenef", nomPatient );
    parametre.addQueryItem ( "numEntree", numDossier );
    parametre.addQueryItem ( "destinataire", mutuelle );
    parametre.addQueryItem ( "emetteur", eds );


    //Pr�paration de la requ�te
    QNetworkAccessManager *m_network;
    m_network = new QNetworkAccessManager(this);

    QNetworkRequest request;
    request.setHeader( QNetworkRequest::ContentTypeHeader, QVariant("application/x-www-form-urlencoded") );
    request.setRawHeader ( HTTP_DOMAINE_4AXES.toAscii (), HTTP_DOMAINE_4AXES_VALEUR.toAscii () ); //S�curit� -> header
    request.setUrl( QUrl ( ADR_HTTP_RECHERCHE ) );

    ui->statusBar->showMessage ("Requ�te sur " % ADR_HTTP_RECHERCHE);

    QNetworkReply *reply = m_network->post( request, parametre.encodedQuery () );

    //Connexion r�ponse
    connect(m_network,
            SIGNAL( finished(QNetworkReply *) ),
            this,
            SLOT( retour_recherche(QNetworkReply *) ) );

    connect(reply,
            SIGNAL(error(QNetworkReply::NetworkError)),
            this,
            SLOT(messageErreur(QNetworkReply::NetworkError)));

    connect(reply,
            SIGNAL(downloadProgress(qint64,qint64)),
            this,
            SLOT(progressionTelechargement(qint64, qint64) ) );

}



















//----------------------------------------------------------------------------------------------------------------------
/*
  Permet d'envoyer un fichier PDF
  remplace la RPEC identifi�e par son ID (idRPEC)
  */
void MainWindow::envoyerSwitch (QString idRPEC){

    QFile fichier;
    if( !fichier.exists ( pathFichier ) ){

        ecrireLog (NOM_FICHIER_LOG,"Le fichier a envoy� n'existe pas ou a �t� d�plac� - R�conciliation annul�e");
        ecrireLog (NOM_FICHIER_LOG,"-----------------------------------");
        debugger ("Envoi fichier","Le fichier a envoy� n'existe pas ou a �t� d�plac� - R�conciliation annul�e");
        ui->zoneTexte->setText ("Le fichier " % pathFichier % " n'existe pas ou a �t� d�plac� - R�conciliation annul�e");
    }
    else{


        time->start ();

        ecrireLog (NOM_FICHIER_LOG,"Envoi du fichier pour SWITCH sur " % ADR_HTTP_ENVOYER_SWITCH % " a l'entite " % idRPEC);

        QProcess processus;

        ui->statusBar->showMessage ("Requ�te sur " % ADR_HTTP_ENVOYER_SWITCH);

        //Lancer CURL avec idMesage / pathFichier / rnm mutuelle en cours / url 4axes.net
        QString cmd = QCoreApplication::applicationDirPath ()
                % "/CURL/curl.exe -k -H " % HTTP_DOMAINE_4AXES % ":" % HTTP_DOMAINE_4AXES_VALEUR
                % " --form idMessage="% idRPEC
                % " --form \"fichier=@" % pathFichier % "\" " % ADR_HTTP_ENVOYER_SWITCH;

        //Lancement de la commande avec cUrl.exe
        processus.start (cmd);

        //On lit la sortie pour connaitre le resultat de notre requ�te
        processus.setProcessChannelMode(QProcess::MergedChannels);
        processus.waitForFinished(-1);
        QString resultatPrompt = processus.readAll ();

        if( resultatPrompt.isEmpty () )
            ecrireLog (NOM_FICHIER_LOG,"Erreur processus : " % processus.errorString ());

        //Affichage de la r�ponse
        ui->zoneTexte->setText (resultatPrompt);

        //Analyse de la r�ponse
        parserReponse (resultatPrompt, typeContexte.switchRPEC );
    }

}
















//----------------------------------------------------------------------------------------------------------------------
/*
  R�cup�re les informations de la RPEC en fonction de son idMessage.
  1er param�tre : idMessage concern�
  2e message : Nom de la/les colonnes � s�lectionner (colonne statut ici)

  NOTE : Si plusieurs colonnes a s�lectionner alors s�parez chaque nom de colonne par un ";"

  S�curit� : header personnalis� dans la requ�te -> Key : domaine / valeur : 4axes
  */
void MainWindow::getStatutRPEC (QString idMessage){

    if( !idMessage.isEmpty () ){

        time->start ();

        ecrireLog (NOM_FICHIER_LOG,"Lancement requ�te sur " % ADR_HTTP_GETMESSAGE % " idMessage -> " % idMessage);

        QUrl parametre;
        parametre.addQueryItem ( "idMessage", idMessage );
        parametre.addQueryItem ( "colonnes", "statut");

        //Pr�paration de la requ�te
        QNetworkAccessManager *m_network;
        m_network = new QNetworkAccessManager(this);

        QNetworkRequest request;
        request.setHeader( QNetworkRequest::ContentTypeHeader, QVariant("application/x-www-form-urlencoded") );
        request.setRawHeader ( HTTP_DOMAINE_4AXES.toAscii (), HTTP_DOMAINE_4AXES_VALEUR.toAscii () );    //S�curit� -> header
        request.setUrl( QUrl ( ADR_HTTP_GETMESSAGE ) );

        ui->statusBar->showMessage ("Requ�te sur " % ADR_HTTP_GETMESSAGE);

        QNetworkReply *reply = m_network->post( request, parametre.encodedQuery () );

        //Connexion r�ponse
        connect(m_network,
                SIGNAL( finished(QNetworkReply *) ),
                this,
                SLOT( retour_getStatutRPEC(QNetworkReply *) ) );
    }
}




























//----------------------------------------------------------------------------------------------------------------------
/*
  R�cup�re les donn�es concernant le statut d'un retour de prise en charge
  */
void MainWindow::retour_getStatutRPEC (QNetworkReply *reply){

    if (reply->error() > 0){

        ui->zoneTexte->setPlainText (reply->errorString());
        ecrireLog (NOM_FICHIER_LOG,"Erreur phase 4 : " % reply->errorString());
    }
    else {
        QByteArray data = reply->readAll();

        if ( data.length () > 0 ){

            ui->zoneTexte->setPlainText ("R�ponse OK");
            parserReponse ( data, typeContexte.statutRPEC );
        }
    }
}




























//----------------------------------------------------------------------------------------------------------------------
void MainWindow::debugger (QString titre, QString info){

    QMessageBox::critical (NULL,titre,info);
}


//----------------------------------------------------------------------------------------------------------------------
void MainWindow::debuggerInfo (QString titre, QString info){

    QMessageBox::information (NULL,titre,info);
}






























//----------------------------------------------------------------------------------------------------------------------
/*
  Permet d'alimenter un fichier de trace/log dont le nom et les informations sont pass�s en param�tre
  */
void MainWindow::ecrireLog (QString nomFichier, QString info){

    QFile fichierLog(QCoreApplication::applicationDirPath () % "/" + nomFichier);
    if( ! fichierLog.open ( QIODevice::Text | QIODevice::WriteOnly | QIODevice::Append ) )
        debugger ("Ouverture fichier log","Impossible d'ouvrir le fichier " % nomFichier % " - " % fichierLog.errorString ());

    else{

        QDateTime dateTime;

        QTextStream fluxLog(&fichierLog);

        fluxLog << dateTime.currentDateTime ().toString ("dd/MM/yyyy hh:mm:ss")
                    << " >> "
                    << info
                    << endl;

        fichierLog.close ();
    }
}



























//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur clic sur un item dans la liste les informations s'affiche dans les QLineEdit correspondant
  */
void MainWindow::on_listWidget_DPEC_itemClicked(QListWidgetItem *item){

    QPalette* palette = new QPalette();

    if( importationEDS ){

        QStringList element;

        ui->radioButton_reconcilier->setEnabled (false);
        ui->radioButton_erreurDest->setEnabled (false);
        ui->radioButton_ArchivageNR->setEnabled (false);
        ui->radioButton_switch->setEnabled ( false );
        ui->radioButton_ArchivageNT->setEnabled ( false );

        ui->radioButton_envoyerSEC->setAutoExclusive(true);
        ui->radioButton_envoyerSEC->setEnabled (true);

        ui->btn_telechargerDPEC->setEnabled (false);
        ui->btn_telechargerRPEC->setEnabled (false);

        palette->setColor(QPalette::WindowText,Qt::black);
        ui->label_statutDPEC->setPalette (*palette);

        ui->lineEdit_IDMsg->clear ();
        ui->lineEdit_statutDPEC->clear ();
        ui->lineEdit_statutRPEC->clear ();
        ui->lineEdit_emetteur_DPEC->clear ();
        ui->lineEdit_dest_DPEC->clear ();
        ui->lineEdit_dateEntree->clear ();
        ui->lineEdit_prenomPatient->clear ();
        ui->lineEdit_nomPatient->clear ();
        ui->lineEdit_numDossier->clear ();

        for( int i = 0 ; i < listeEDS.length () ; i++ ){

            element = listeEDS.at(i).split (";");

            if( element.at (0) == item->text () ){

                //Affichage de l'�l�ment "nomEDS"
                ui->lineEdit_emetteur_DPEC->insert ( element.at (3) );
                ui->lineEdit_emetteur_DPEC->setToolTip ( element.at (3) );

                //On met le curseur au d�but du QLinedEdit
                ui->lineEdit_emetteur_DPEC->home (false);
            }
        }
        idEDSSelect = item->text ();
    }
    else{

        ui->checkBox_multiReconciliation->setEnabled (true);
        ui->checkBox_nonDeplacement->setEnabled ( true );

        idMsgSelect = item->text ().replace (" ","");

        ui->btn_telechargerDPEC->setEnabled (true);

        //On boucle sur le r�sultat
        for( int i = 0 ; i < listeDPEC.length () ; i++){

            //Si on trouve l'idMessage
            if( listeDPEC[i].idMessageDPEC == idMsgSelect ){

                idEDSSelect = listeDPEC[i].idClientE;

                //On alimente l'IHM
                ui->lineEdit_IDMsg->clear ();
                ui->lineEdit_IDMsg->insert (listeDPEC[i].idMessageDPEC);

                ui->lineEdit_statutDPEC->clear ();
                ui->lineEdit_statutDPEC->insert (listeDPEC[i].statutDPEC);

                ui->lineEdit_statutRPEC->clear ();
                ui->lineEdit_statutRPEC->insert ( listeDPEC[i].statutRPEC );

                ui->lineEdit_emetteur_DPEC->clear ();
                ui->lineEdit_emetteur_DPEC->insert (listeDPEC[i].emetteur);
                ui->lineEdit_emetteur_DPEC->setToolTip (listeDPEC[i].emetteur);
                ui->lineEdit_emetteur_DPEC->home (false);

                ui->lineEdit_dest_DPEC->clear ();
                ui->lineEdit_dest_DPEC->insert (listeDPEC[i].destinataire);
                ui->lineEdit_dest_DPEC->setToolTip (listeDPEC[i].destinataire);
                ui->lineEdit_dest_DPEC->home (false);

                ui->lineEdit_dateEntree->clear ();
                ui->lineEdit_dateEntree->insert (listeDPEC[i].dateEntree);

                ui->lineEdit_nomPatient->clear ();
                ui->lineEdit_nomPatient->insert (listeDPEC[i].nomPatient);

                ui->lineEdit_prenomPatient->clear ();
                ui->lineEdit_prenomPatient->insert (listeDPEC[i].prenomPatient);

                ui->lineEdit_numDossier->clear ();
                ui->lineEdit_numDossier->insert (listeDPEC[i].numDossier);

                //Affichage du bouton "T�l�charger RPEC" selon le statut de la DPEC
                if( listeDPEC[i].statutDPEC == StatutDPEC.acquitte ){

                    //Lancer la requ�te seulement si le statut de la RPEC n'est pas renseigner
                    if( listeDPEC[i].statutRPEC == "N/C" )
                        getStatutRPEC ( listeDPEC[i].idMessageRPEC );

                    ui->btn_telechargerRPEC->setEnabled (true);

                    ui->radioButton_envoyerSEC->setAutoExclusive(true);
                    ui->radioButton_envoyerSEC->setEnabled (true);

                    ui->radioButton_ArchivageNR->setAutoExclusive(true);
                    ui->radioButton_ArchivageNR->setEnabled (true);

                    ui->radioButton_erreurDest->setEnabled (false);
                    ui->radioButton_reconcilier->setEnabled (false);

                    palette->setColor(QPalette::WindowText,Qt::red);
                    ui->label_statutDPEC->setPalette (*palette);
                }
                else if( listeDPEC[i].statutDPEC != StatutDPEC.acquitte ){

                    ui->btn_telechargerRPEC->setEnabled (false);

                    if( listeDPEC[i].statutDPEC == StatutDPEC.purge || listeDPEC[i].statutDPEC == StatutDPEC.annule ){

                        ui->radioButton_erreurDest->setEnabled (false);
                        ui->radioButton_reconcilier->setEnabled (false);
                        ui->radioButton_envoyerSEC->setEnabled (false);

                        ui->radioButton_ArchivageNR->setAutoExclusive(true);
                        ui->radioButton_ArchivageNR->setEnabled (true);
                    }
                    else{

                        ui->radioButton_envoyerSEC->setAutoExclusive(true);
                        ui->radioButton_envoyerSEC->setEnabled (true);

                        ui->radioButton_erreurDest->setAutoExclusive(true);
                        ui->radioButton_erreurDest->setEnabled (true);

                        ui->radioButton_reconcilier->setAutoExclusive(true);
                        ui->radioButton_reconcilier->setEnabled (true);

                        ui->radioButton_ArchivageNR->setAutoExclusive(true);
                        ui->radioButton_ArchivageNR->setEnabled (true);
                    }

                    //On passe le label "Statut DPEC" rouge
                    palette->setColor(QPalette::WindowText,Qt::black);
                    ui->label_statutDPEC->setPalette (*palette);
                }
            }
        }
    }
}
























//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur coche/d�coche le QCheckBox
  */
void MainWindow::on_checkBox_multiReconciliation_clicked(){

    if( ui->checkBox_multiReconciliation->isChecked () ){

        ui->checkBox_selectionnerTout->setVisible ( true );
        remplirTableauCheckBox (listeDPEC, false);
    }
    else if ( !ui->checkBox_multiReconciliation->isChecked () ){

        ui->checkBox_selectionnerTout->setVisible ( false );
        remplirTableau (listeDPEC);
    }
}































//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur clic sur le bouton "telecharger RPEC"
  Permet de t�l�charger le message RPEC

  S�curit� : header personnalis� dans la requ�te -> Key : domaine / valeur : 4axes
  */
void MainWindow::on_btn_telechargerRPEC_clicked(){

    if( !idMsgSelect.isEmpty () ){

        ecrireLog ( NOM_FICHIER_LOG, "Action -> T�l�charger RPEC");

        //On r�cup�re idMessageRPEC correspondant � l'idMessageDPEC dans la liste de structure
        QString idMsgRPEC = getIdMsgRPEC( idMsgSelect, listeDPEC );

        //Cr�ation param�tre
        QUrl parametre;
        parametre.addQueryItem ("idMessage",idMsgRPEC);
        parametre.addQueryItem ("type","document");
        //TODO -> ajouter nouveau param�tre pour ne pas changer le statut du message t�l�charg�

        ecrireLog (NOM_FICHIER_LOG,"Telechargement RPEC ( " % idMsgRPEC % " ) depuis " % ADR_HTTP_DOWNLOAD);

        //Initalisation requ�te
        QNetworkRequest requete;
        requete.setHeader( QNetworkRequest::ContentTypeHeader, QVariant("application/x-www-form-urlencoded") );
        requete.setRawHeader ( HTTP_DOMAINE_4AXES.toAscii (), HTTP_DOMAINE_4AXES_VALEUR.toAscii () );  //S�curit� -> header
        requete.setUrl( QUrl ( ADR_HTTP_DOWNLOAD ) );

        ui->statusBar->showMessage ("Requ�te sur " % ADR_HTTP_DOWNLOAD);

        //Cr�ation manager r�seau
        QNetworkAccessManager *m_network = new QNetworkAccessManager;

        //Cr�ation de la r�ponse
        QNetworkReply *reply = m_network->post(requete, parametre.encodedQuery ());

        //Connexion signal -> SLOT
        connect(m_network, SIGNAL( finished(QNetworkReply *) ),this, SLOT( retour_telechargerFichier(QNetworkReply *) ) );
    }
    else
        debugger ("S�lection DPEC","Veuillez s�lectionner une DPEC !");
}





























//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur clic sur le bouton "telecharger DPEC"
  Permet de t�l�charger le message DPEC

  S�curit� : header personnalis� dans la requ�te -> Key : domaine / valeur : 4axes
  */
void MainWindow::on_btn_telechargerDPEC_clicked(){

    if( !idMsgSelect.isEmpty () ){

        ecrireLog ( NOM_FICHIER_LOG, "Action -> T�l�charger DPEC");

        //Cr�ation param�tre
        QUrl parametre;
        parametre.addQueryItem ("idMessage",idMsgSelect);
        parametre.addQueryItem ("type","document");
        //TODO -> ajouter nouveau param�tre pour ne pas changer le statut du message t�l�charg�

        ecrireLog (NOM_FICHIER_LOG,"Telechargement RPEC ( " % idMsgSelect % " ) depuis " % ADR_HTTP_DOWNLOAD);

        //Initalisation requ�te
        QNetworkRequest requete;
        requete.setHeader( QNetworkRequest::ContentTypeHeader, QVariant("application/x-www-form-urlencoded") );
        requete.setRawHeader ( HTTP_DOMAINE_4AXES.toAscii (), HTTP_DOMAINE_4AXES_VALEUR.toAscii () );   //S�curit� -> header
        requete.setUrl( QUrl ( ADR_HTTP_DOWNLOAD ) );

        ui->statusBar->showMessage ("Requ�te sur " % ADR_HTTP_DOWNLOAD);

        //Cr�ation manager r�seau
        QNetworkAccessManager *m_network = new QNetworkAccessManager;

        //Cr�ation de la r�ponse
        QNetworkReply *reply = m_network->post(requete, parametre.encodedQuery ());

        //Connexion signal -> SLOT
        connect(m_network, SIGNAL( finished(QNetworkReply *) ),this, SLOT( retour_telechargerFichier(QNetworkReply *) ) );
    }
    else
        debugger ("S�lection DPEC","Veuillez s�lectionner une DPEC !");

}























//----------------------------------------------------------------------------------------------------------------------
/*
  Une fois que le serveur a emit une r�ponse
  */
void MainWindow::retour_telechargerFichier (QNetworkReply *reply){

    if (reply->error() > 0){

        ui->zoneTexte->setPlainText (reply->errorString());
        ecrireLog (NOM_FICHIER_LOG,"Erreur phase 3 : " % reply->errorString() );
    }
    else {

        QByteArray data = reply->readAll();

        ecrireLog ("download.log",data);

        //Cr�ation du fichier PDF
        QFile fichierPDF(QCoreApplication::applicationDirPath () % "/" % NOM_FICHIER_RECEPTION);

        if ( !fichierPDF.open(QIODevice::WriteOnly) ){
            debugger ("T�l�chargement RPEC","Impossible d'ouvrir le fichier de r�ception : "
                      % NOM_FICHIER_RECEPTION
                      % " -> " % fichierPDF.errorString () );
        }
        else{

            //On �cris dans le fichier
            fichierPDF.write(data);
            fichierPDF.close();
            reply->deleteLater();

            ui->zoneTexte->setPlainText ("T�l�chargement termin� !");

            QDesktopServices::openUrl ( QUrl( "file:///" % QCoreApplication::applicationDirPath ()
                                              % "/" % NOM_FICHIER_RECEPTION ) );
        }
    }
}
























//----------------------------------------------------------------------------------------------------------------------
/*
  Retourne l'ID message de la RPEC pr�sent dans la structure de donn�e identifi�e par l'ID message de la DPEC
  */
QString MainWindow::getIdMsgRPEC (QString idMessageDPEC, QList<DPEC> listeStruct){

    for( int i = 0 ; i < listeStruct.length () ; i++ ){

        if( listeStruct[i].idMessageDPEC == idMessageDPEC )
            return( listeStruct[i].idMessageRPEC);
    }
    return("NULL");
}



















//----------------------------------------------------------------------------------------------------------------------
/*
    Recherche dans la liste de structure DPEC si parmis la liste de DPEC � r�concilier il y a une DPEC avec
    un statut ACQUITTE
  */
bool MainWindow::verifierStatut (QStringList listeDPECRecon, QList<DPEC> listeStruct){

    for( int i = 0 ; i < listeDPECRecon.length () ; i++ ){

        for( int j = 0 ; j < listeStruct.length () ; j++ ){

            if( listeStruct[j].idMessageDPEC == listeDPECRecon.at (i) ){

                if( listeStruct[j].statutDPEC == StatutDPEC.acquitte
                        ||  listeStruct[j].statutDPEC == StatutDPEC.annule
                        || listeStruct[j].statutDPEC == StatutDPEC.purge )
                    return(true);
            }
        }
    }
    return(false);
}


















//----------------------------------------------------------------------------------------------------------------------
/*
  Importe la liste des EDS (<idEDS;nomEds>) depuis un fichier format CSV
  Et remplit le liste avec les idEDS

  S�curit� : header personnalis� dans la requ�te -> Key : domaine / valeur : 4axes
  */
void MainWindow::on_btn_importerEDS_clicked(){

    ecrireLog ( NOM_FICHIER_LOG, "Action -> importer EDS");

    //Initalisation requ�te
    QNetworkRequest requete;
    requete.setHeader( QNetworkRequest::ContentTypeHeader, QVariant("application/x-www-form-urlencoded") );
    requete.setRawHeader ( HTTP_DOMAINE_4AXES.toAscii () , HTTP_DOMAINE_4AXES_VALEUR.toAscii () );   //S�curit� -> header
    requete.setUrl( QUrl ( ADR_HTTP_DOWNLOAD_EDS ) );

    ui->statusBar->showMessage ("Requ�te sur " % ADR_HTTP_DOWNLOAD_EDS);

    //Cr�ation manager r�seau
    QNetworkAccessManager *m_network = new QNetworkAccessManager;

    //Cr�ation de la r�ponse
    QNetworkReply *reply = m_network->get(requete);

    ecrireLog (NOM_FICHIER_LOG, "Lancement requete importation liste EDS sur " % ADR_HTTP_DOWNLOAD_EDS);

    //Connexion signal -> SLOT
    connect(m_network, SIGNAL( finished(QNetworkReply *) ),this, SLOT( retour_telechargerFichierEDS(QNetworkReply *) ) );
}


































//----------------------------------------------------------------------------------------------------------------------
/*
  Permet d'importer un fichier local au format CSV contenant la liste des EDS
  */
void MainWindow::importationLocalEDS (QString nomFichierEDS){

    ecrireLog (NOM_FICHIER_LOG,"Importation de la liste des EDS");

    //On ouvrir le fichier pour int�grer les donn�es dans l'IHM
    QFile fichierEDS( QCoreApplication::applicationDirPath () % "\\" % nomFichierEDS);
    if( ! fichierEDS.open ( QIODevice::Text | QIODevice::ReadWrite ) )
        debugger ("Ouverture fichier EDS CSV","Impossible d'ouvrir le fichier CSV des �tablissements de soin");
    else{

        ui->checkBox_multiReconciliation->setEnabled (false);

        //On efface la liste des EDS avant la r�utilisation
        listeEDS.clear ();

        //On active le flag pour informer qu'on a actionner l'importation des EDS
        importationEDS = true;

        //Cr�ation du flux
        QTextStream fluxCSV(&fichierEDS);

        QString enreg;
        QString ligneCourante;

        //Tant qu'on est pas arriv� � la fin du fichier
        while( ! fluxCSV.atEnd () ){

            //Lecture ligne
            ligneCourante = fluxCSV.readLine ();

            //Si ligne non vide
            if( ! ligneCourante.isEmpty () )
                listeEDS.push_back ( ligneCourante ); //On ajout chaque ligne dans la liste
        }

        //Une fois finit on ferme le fichier
        fichierEDS.close ();

        //On efface la liste IHM pour la r�alimenter
        ui->listWidget_DPEC->clear ();

        //On charge l'IHM
        for( int i = 0 ; i < listeEDS.length () ; i++ ){

            enreg = listeEDS.at (i);
            QListWidgetItem *item = new QListWidgetItem( enreg.mid ( 0, enreg.indexOf (";") ) );

            //On ajoute un item � la liste
            ui->listWidget_DPEC->addItem ( item );
        }
    }
}























//----------------------------------------------------------------------------------------------------------------------
/*
  SLOT : r�cup�re la r�ponse du serveur concernant la liste des EDS r�f�renc�
  pour l'importer dans l'IHM
  */
void MainWindow::retour_telechargerFichierEDS (QNetworkReply *reply){

    //Si erreur
    if (reply->error() > 0){

        ui->zoneTexte->setPlainText (reply->errorString());
        ecrireLog (NOM_FICHIER_LOG,"Erreur phase 3 : " % reply->errorString() );
    }
    else {

        //Lecture du retour
        QByteArray data = reply->readAll();

        //On met le retour tel quel dans un fichier au cas o� nous devons voir ce que le serveur retourne
        ecrireLog ("download.log",data);

        //Cr�ation du fichier PDF
        QFile fichierPDF(QCoreApplication::applicationDirPath () % "/" % NOM_FICHIER_RECEPTIONEDS);

        if ( !fichierPDF.open(QIODevice::WriteOnly) ){
            debugger ("T�l�chargement RPEC","Impossible d'ouvrir le fichier de r�ception : "
                      % NOM_FICHIER_RECEPTION
                      % " -> " % fichierPDF.errorString () );
        }
        else{

            ecrireLog (NOM_FICHIER_LOG, "Importation de la liste des EDS");

            //On �cris dans le fichier
            fichierPDF.write(data);
            fichierPDF.close();
            reply->deleteLater();

            ui->zoneTexte->setPlainText ("T�l�chargement termin� !");

            //On ouvrir le fichier pour int�grer les donn�es dans l'IHM
            QFile fichierEDS( QCoreApplication::applicationDirPath () % "\\" % NOM_FICHIER_RECEPTIONEDS);
            if( ! fichierEDS.open ( QIODevice::Text | QIODevice::ReadWrite ) )
                debugger ("Ouverture fichier EDS CSV","Impossible d'ouvrir le fichier CSV des �tablissements de soin");
            else{

                ui->checkBox_multiReconciliation->setEnabled (false);
                ui->checkBox_nonDeplacement->setEnabled (false);

                //On effacte la liste des EDS avant la r�utilisation
                listeEDS.clear ();

                //On active le flag pour informer qu'on a actionner l'importation des EDS
                importationEDS = true;

                //Cr�ation du flux
                QTextStream fluxCSV(&fichierEDS);

                QString enreg;
                QString ligneCourante;

                //Tant qu'on est pas arriv� � la fin du fichier
                while( ! fluxCSV.atEnd () ){

                    //Lecture ligne
                    ligneCourante = fluxCSV.readLine ();

                    //Si ligne non vide
                    if( ! ligneCourante.isEmpty () )
                        listeEDS.push_back ( ligneCourante ); //On ajout chaque ligne dans la liste

                }

                //Une fois finit on ferme le fichier
                fichierEDS.close ();

                //On efface la liste IHM pour la r�alimenter
                ui->listWidget_DPEC->clear ();

                //On charge l'IHM
                for( int i = 0 ; i < listeEDS.length () ; i++ ){

                    enreg = listeEDS.at (i);
                    QListWidgetItem *item = new QListWidgetItem( enreg.mid ( 0, enreg.indexOf (";") ) );

                    //On ajoute un item � la liste
                    ui->listWidget_DPEC->addItem ( item );
                }
            }
        }
    }
}






















//----------------------------------------------------------------------------------------------------------------------
/*
  Progression de la bar de progression
  */
void MainWindow::progressionTelechargement (qint64 bytesReceived, qint64 bytesTotal){

    if (bytesTotal != -1){

        ui->progressBar->setRange(0, bytesTotal);
        ui->progressBar->setValue(bytesReceived);
    }
}





















//----------------------------------------------------------------------------------------------------------------------
/*
  Ev�nement sur l'action "Entrer" / "Return" / "F5" sur clavier
  */
void MainWindow::keyPressEvent (QKeyEvent *e){

    if( e->key () == Qt::Key_Enter || e->key () == Qt::Key_Return )
        on_btn_rechercher_clicked ();

    else if ( e->key () == Qt::Key_F5 )
        listerFichier ( pathMutuelleEnCours, false );
}

















//----------------------------------------------------------------------------------------------------------------------
/*
  Affiche les fichiers du r�pertoire dans le Widget : listWidget_fichierPDF
  */
void MainWindow::listerFichier (QString path, bool checkBox){

    if( ! path.isEmpty () ){

        ui->listWidget_fichierPDF->clear ();

        QStringList element = path.split ("\\");
        QString branche;

        if( path.contains ( structPopper.good ) ){ //Si GOOD

            mutuelleEnCours = element.at ( element.length () - 2 );
            branche = element.at ( element.length () - 3 );
        }
        else if( path.contains ( structPopper.unknown ) ){ //Si UNKNOWN

            mutuelleEnCours =  element.at ( element.length () - 1 );
            branche = element.at ( element.length () - 2 );
        }

        ui->label_mutuelleEnCours->setText ("Mutuelle en cours : " %  mutuelleEnCours
                                            % " | ---> " % branche);

        //It�rateur sur les fichiers
        QDirIterator iterateurFichier( path, QDir::Files | QDir::NoDotAndDotDot | QDir::NoSymLinks );

        QFileInfo fichierInfo;

        //Tant qu'on est pas arriv� � la fin du r�pertoire
        while( iterateurFichier.hasNext () ){

            fichierInfo = iterateurFichier.next ();

            //Ajout de l'item dans la liste
            QListWidgetItem *item = new QListWidgetItem( fichierInfo.fileName ()
                                                         % " # ( " % QString::number ( fichierInfo.size () / 1024 )
                                                         % " Ko )");

            //On ajoute un item � la liste
            ui->listWidget_fichierPDF->addItem ( item );

            if( checkBox )
                ui->listWidget_fichierPDF->item ( ui->listWidget_fichierPDF->row ( item ) )->setCheckState ( Qt::Unchecked );
        }
    }
}































//----------------------------------------------------------------------------------------------------------------------
/*
  Permet de lister le dossier TEMP (TMP_xxxxxxxxx_nomMutuelle)
  */
void MainWindow::listerTMP (QString path){

    if( !path.isEmpty () ){

        ui->comboBox_TMP->clear ();

        //Iterateur sur dossier pour d�tecteur les dossier TMP
        QDirIterator iterateurDossierTMP( path, QDir::Dirs | QDir::NoDotAndDotDot | QDir::NoSymLinks );

        QDir dossier;
        QStringList listeDossierTMP;

        //Tant qu'on est pas arriv� � la fin du r�pertoire
        while( iterateurDossierTMP.hasNext () ){

            dossier = iterateurDossierTMP.next ();

            //Si le dossier en cours contient "TMP" sur les 3 premiers caract�res
            if( dossier.dirName ().mid (0,3) == "TMP" )
                listeDossierTMP.push_back ( dossier.dirName () );
        }

        listeDossierTMP.removeDuplicates ();

        //Ajout � la comboBox
        ui->comboBox_TMP->addItems ( listeDossierTMP );
    }

}











//----------------------------------------------------------------------------------------------------------------------
/*
  Permet de lister le nom des VM dans le combobox d�di�e
  */
void MainWindow::listerVM(){

    ui->comboBox_autresMutuelles->addItems ( config->getListKey ( QCoreApplication::applicationDirPath () + "\\config.ini", "VM") );
}

























//----------------------------------------------------------------------------------------------------------------------
/*
  SLOT : Lorsque l'utilisateur clique sur un �l�ment de la liste : listWidget_fichierPDF
  */
void MainWindow::on_listWidget_fichierPDF_itemClicked(QListWidgetItem *item){


}




//----------------------------------------------------------------------------------------------------------------------
/*
  SLOT : Lorsque l'utilisateur effectue un double clic sur un �l�ment de la liste : listWidget_fichierPDF
  */
void MainWindow::on_listWidget_fichierPDF_itemDoubleClicked(QListWidgetItem *item){

    if( !item->text ().isEmpty () ){

        importationEDS = false;

        pdfSelect = item->text ().mid (0, item->text ().indexOf ("#") - 1);
        QFileInfo pdfSelectInfo(pdfSelect);

        if( pdfSelectInfo.suffix () != "pdf" ){

            QString fichierARenommer = item->text ().mid (0, item->text ().indexOf ("#") - 1 );

            QFile fichier(pathMutuelleEnCours % "/" % fichierARenommer);

            if( !fichier.rename ( pathMutuelleEnCours % "/" % fichierARenommer % ".pdf" ) )
                debuggerInfo ("Renommage PDF","Impossible de renommer le fichier PDF " % fichierARenommer
                              % " | Erreur : " % fichier.errorString () );
            else
                listerFichier ( pathMutuelleEnCours, false );
        }

        pathFichier = pathMutuelleEnCours % "\\" % pdfSelect;

        //On ouvre le fichier automatiquement
        QDesktopServices::openUrl(QUrl("file:///" % pathMutuelleEnCours % "\\" % pdfSelect , QUrl::TolerantMode) );

        //On r�cup�re le rnm & on l'affiche
        rnm_courant = mutuelleEnCours.mid ( 0, mutuelleEnCours.indexOf ("_") );
        ui->lineEdit_rnmMutuelle_rche->setText ( rnm_courant );

        //Lancement de la requ�te SI et SEULEMENT on se trouve dans GoodNIN
        if( pathMutuelleEnCours.contains ("GoodNIN") )
            //Lancement de la requete PHASE 1 (recherche)
            lancerRequete( pdfSelectInfo );
    }
    else
        debuggerInfo ("Nom PDF vide","Le non du fichier PDF est vide");
}

















//----------------------------------------------------------------------------------------------------------------------
/*
  SLOT : Lorsque l'utilisateur clique sur le bouton : btn_deplacerTMP
  */
void MainWindow::on_btn_deplacerTMP_clicked(){

    ecrireLog ( NOM_FICHIER_LOG, "Action -> D�placer TMP");

    if( !ui->comboBox_TMP->currentText ().isEmpty () || !pdfSelect.isEmpty () ){

        //Si on a importer la liste des mutuelles de la mutelle concern�e
        if( siVmImporter ){

            deplacerFichierTMP ( "\\\\"
                                 % config->getValue ( QCoreApplication::applicationDirPath () % "/config.ini",
                                                             "VM", ui->comboBox_autresMutuelles->currentText () )
                                 % "/" % config->getValue ( QCoreApplication::applicationDirPath () % "/config.ini",
                                                            "Path", "GoodNIN")
                                 % "/" % ui->comboBox_TMP->currentText () );

            siVmImporter = false;
            listerTMP ( pathMutuelleEnCours );
        }
        else
            deplacerFichierTMP ( pathMutuelleEnCours % "/" % ui->comboBox_TMP->currentText () );
    }
    else
        debuggerInfo ("S�lection TMP ou fichier PDF","Veuillez s�lectionner un dossier TMP ou une RPEC � d�placer");
}

























//----------------------------------------------------------------------------------------------------------------------
/*
  Permet de d�placer un fichier dans un dossier TEMP (TMP)
  */
void MainWindow::deplacerFichierTMP(QString TMPDestination){

    fermerFichierPDF ();

    QDir dossier(TMPDestination);

    QStringList listeNomFichierTMP;

    //On r�cup�re la liste des fichiers s�lectionn�
    if( ui->checkBox_multiSelection->isChecked () )
        compterFichierSelectionner( listeNomFichierTMP );


    //V�rifier qu'il y a soit un fichier PDF de s�lection soit un ensemble
    if( !pathFichier.isEmpty () || !listeNomFichierTMP.isEmpty () ){

        //Si le dossier de destination existe
        if( dossier.exists () ){

            //Si on a coch� la multi s�lection
            if( ui->checkBox_multiSelection->isChecked () ){

                bool fichierDeplacer = false;

                QString pathFichierMulti;
                QString nomFichierCoupe;

                //On parcours notre liste de fichier s�lectionn�
                for( int i = 0 ; i < listeNomFichierTMP.length () ; i++ ){

                    //Vu qu'on r�cup�re le nom de l'objet qu'on s�lectionn� sur l'IHM
                    //il est n�cessaire de r�cup�rer que le nom exacte du fichier
                    //et non le "nom_du_fichier # (sa_taille_en_ko)"
                    nomFichierCoupe = listeNomFichierTMP.at (i).mid (0, listeNomFichierTMP.at (i).indexOf ("#") - 1 );

                    //On recompose le path du fichier [ path dossier origine + nom du fichier ]
                    pathFichierMulti = pathMutuelleEnCours % "\\" % nomFichierCoupe;
                    QListWidgetItem *item = new QListWidgetItem( listeNomFichierTMP.at (i) );

                    //On m�morise la valeur de retour lors du d�placement fichier
                    fichierDeplacer = deplacementFichier ( dossier.absolutePath () , pathFichierMulti );
                    if ( fichierDeplacer ){

                        //On supprime l'objet sur l'IHM + Refresh
                        delete item;
                        on_btn_actualiserPDF_clicked ();
                    }
                }

                //Infi IHM si tous les d�placements se sont bien d�roul�s
                if( fichierDeplacer )
                    debuggerInfo ("Succ�s", "Les fichiers ont �t� d�plac�");

                //Une fois qu'il n'y a plus de fichier dans le dossier
                if( ui->listWidget_fichierPDF->count () == 0 )
                    QCoreApplication::quit ();
            }
            else if( !ui->checkBox_multiSelection->isChecked () ){          //Si on a pas s�lectionn� la multi-s�lection

                //On test la valeur de retour du d�placement fichier
                if( deplacementFichier ( TMPDestination, pathMutuelleEnCours % "/" % pdfSelect ) ){

                    //On supprime l'objet sur l'IHM
                    delete ui->listWidget_fichierPDF->currentItem ();
                    debuggerInfo ("Succ�s","Fichier d�plac�");

                    //R�initialisation de l'IHM
//                    viderWidget();

                    //Une fois qu'il n'y a plus de fichier dans le dossier
                    if( ui->listWidget_fichierPDF->count () == 0 )
                        QCoreApplication::quit ();
                }
            }
        }
        else
            debuggerInfo ("Dossier inconnu TMP","Le dossier de destination : " % TMPDestination % " n'existe pas" );
    }
    else
        debuggerInfo ("S�lection information TMP","Veuillez s�lectionner un fichier PDF � d�placer");
}






















//----------------------------------------------------------------------------------------------------------------------
/*
  Vide les champs et d�sactiver les actions
  */
void MainWindow::viderWidget (){

    QPalette* palette = new QPalette();
    palette->setColor(QPalette::WindowText,Qt::black);
    ui->label_statutDPEC->setPalette (*palette);

    ui->label_nbDPEC->clear ();

    siVmImporter = false;

    ui->listWidget_DPEC->clear ();
    ui->zoneTexte->clear ();

    //LineEdit mot de passe pour switch
    ui->lineEdit_mdpSWITCH->setEnabled (false);

    //Bouton
    ui->btn_telechargerDPEC->setEnabled (false);
    ui->btn_telechargerRPEC->setEnabled (false);

    ui->lineEdit_nomEDS->clear ();
    ui->lineEdit_nomPatient_rche->clear ();
    ui->lineEdit_numDossier_rche->clear ();
    ui->lineEdit_numDossier_rche->clear ();
    ui->lineEdit_rnmMutuelle_rche->clear ();

    //Partie info DPEC
    ui->lineEdit_IDMsg->clear ();
    ui->lineEdit_statutDPEC->clear ();
    ui->lineEdit_statutRPEC->clear ();
    ui->lineEdit_emetteur_DPEC->clear ();
    ui->lineEdit_dest_DPEC->clear ();
    ui->lineEdit_dateEntree->clear ();
    ui->lineEdit_nomPatient->clear ();
    ui->lineEdit_numDossier->clear ();
    ui->lineEdit_prenomPatient->clear ();

    //CheckBox
    ui->checkBox_multiReconciliation->setChecked ( false );
    ui->checkBox_nonDeplacement->setChecked ( false );

    /* R�initialise tous les radiobutton*/
    ui->radioButton_ArchivageNR->setAutoExclusive(false);
    ui->radioButton_ArchivageNR->setChecked ( false );
    ui->radioButton_ArchivageNR->setEnabled ( true );

    ui->radioButton_ArchivageNT->setAutoExclusive(false);
    ui->radioButton_ArchivageNT->setChecked ( false );
    ui->radioButton_ArchivageNT->setEnabled ( true );

    ui->radioButton_envoyerSEC->setAutoExclusive(false);
    ui->radioButton_envoyerSEC->setChecked ( false );
    ui->radioButton_envoyerSEC->setEnabled ( true );

    ui->radioButton_erreurDest->setAutoExclusive(false);
    ui->radioButton_erreurDest->setChecked ( false );
    ui->radioButton_erreurDest->setEnabled ( true );

    ui->radioButton_reconcilier->setAutoExclusive(false);
    ui->radioButton_reconcilier->setChecked ( false );
    ui->radioButton_reconcilier->setEnabled ( true );

    ui->radioButton_switch->setAutoExclusive(false);
    ui->radioButton_switch->setChecked ( false );
    ui->radioButton_switch->setEnabled ( true );

    ui->radioButton_ArchivageNT->setAutoExclusive(true);
    ui->radioButton_ArchivageNT->setChecked ( false );
    ui->radioButton_ArchivageNT->setEnabled ( true );

    //Switch
    ui->lineEdit_mdpSWITCH->clear ();
    ui->lineEdit_mdpSWITCH->setVisible ( false );

    //Check box "Tout"
    ui->checkBox_selectionnerTout->setChecked ( false );
    ui->checkBox_selectionnerTout->setVisible ( false );

}


















//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur souhaite supprimer un fichier s�lectionner sur l'IHM
  */
void MainWindow::on_btn_supprimer_clicked(){

    ecrireLog ( NOM_FICHIER_LOG, "Action -> Supprimer");

    fermerFichierPDF ();

    QStringList listeNomFichier;

    //On test si on activer la multi-s�lection
    if( ui->checkBox_multiSelection->isChecked () )
        compterFichierSelectionner( listeNomFichier );

    //On test s'il y a un fichier ou plusieurs fichiers s�lectionn�
    if( !pathFichier.isEmpty () || !listeNomFichier.isEmpty () ){

        //Demande de confirmation
        if( QMessageBox::Yes == QMessageBox::question (NULL,"Confirmation de suppression","Voulez-vous supprimer le ou les fichiers "
                                                       % pdfSelect % " ?",QMessageBox::Yes | QMessageBox::No) ){

            //Si multi-s�lection
            if( ui->checkBox_multiSelection->isChecked () ){

                QString pathFichierMulti;
                QString nomFichierCoupe;

                //On parcours notre liste de fichiers s�lectionn�s
                for( int i = 0 ; i < listeNomFichier.length () ; i++ ){

                    nomFichierCoupe = listeNomFichier.at (i).mid (0, listeNomFichier.at (i).indexOf ("#") - 1 );

                    pathFichierMulti = pathMutuelleEnCours % "\\" % nomFichierCoupe;
                    QListWidgetItem *item = new QListWidgetItem( listeNomFichier.at (i) );

                    //On test la valeur de retour
                    if ( suppressionFichier ( pathFichierMulti ) ){

                        delete item;
                        ecrireLog (NOM_FICHIER_LOG, "Suppression du fichier " % nomFichierCoupe );

                        on_btn_actualiserPDF_clicked ();

                    }
                }
            }
            else{

                //On test la valeur de retour
                if( suppressionFichier ( pathFichier ) ){

                    delete ui->listWidget_fichierPDF->currentItem ();
                    ecrireLog (NOM_FICHIER_LOG, "Suppression du fichier " % pdfSelect );
                }
            }
        }

//        viderWidget ();

        //On ferme l'API s'il n'y a plus de fichier a trait�
        if( ui->listWidget_fichierPDF->count () == 0 )
            QCoreApplication::quit ();
    }
    else
        debuggerInfo ("Suppression impossible","Veuillez s�lectionner un fichier � supprimer");
}






















//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur souhaite ouvrir le dossier de la mutuelle en cours de traitement
  */
void MainWindow::on_btn_ouvrirDossier_clicked(){

    ecrireLog ( NOM_FICHIER_LOG, "Action -> Ouvrir dossier");

    QDesktopServices::openUrl(QUrl("file:///" % pathMutuelleEnCours));

}


















//----------------------------------------------------------------------------------------------------------------------
/*
  SLOT : Lorsque l'utilisateur clique sur le bouton "Actualiser"
    Permet d'actualiser de fa�on manuelle la liste des fichiers PDF affich�e
  */
void MainWindow::on_btn_actualiserPDF_clicked(){

    ecrireLog ( NOM_FICHIER_LOG, "Action -> Bouton actualiser");

//    viderWidget ();
    if( ui->checkBox_multiSelection->isChecked () )
        listerFichier ( pathMutuelleEnCours, true);
    else
        listerFichier ( pathMutuelleEnCours, false);
}


















//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur se change de ligne dans la liste de DPEC
  M�thode d�velopp�e pour que l'utilisateur puisse utiliser les fl�ches HAUT/BAS du clavier
  */
void MainWindow::on_listWidget_DPEC_currentTextChanged(const QString &currentText){

    QListWidgetItem *itemSelect = new QListWidgetItem( currentText );
    on_listWidget_DPEC_itemClicked ( itemSelect );
}






































//----------------------------------------------------------------------------------------------------------------------
/*
  Retourne l'ID du C.H en fonction de l'idMessageDPEC
  */
QString MainWindow::getIdEmetteur (QString idMessageDPEC){

    for( int i = 0 ; i < listeDPEC.length () ; i++ ){

        if( listeDPEC[i].idMessageDPEC == idMessageDPEC )
            return( listeDPEC[i].idMessageDPEC);
    }
    return("NULL");

}
















//----------------------------------------------------------------------------------------------------------------------
/*
  Retourne le nom de la mutuelle de la DPEC en fonction de l'ID de la DPEC
  */
QString MainWindow::getNomMutuelle (QString idMessageDPEC){

    for( int i = 0 ; i < listeDPEC.length () ; i++ ){

        if( listeDPEC[i].idMessageDPEC == idMessageDPEC )
            return( listeDPEC[i].destinataire);
    }
    return("NULL");
}
































//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur s�lection l'action SWITCH
  Le champ de saisie pour le mot de passe s'active
  */
void MainWindow::on_radioButton_switch_clicked(){

    ui->lineEdit_mdpSWITCH->setEnabled (true);
    ui->lineEdit_mdpSWITCH->setVisible (true);


}


















//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur clique sur le radioButton "R�concilier"
  */
void MainWindow::on_radioButton_reconcilier_clicked(){

    ui->lineEdit_mdpSWITCH->setVisible (false);
    ui->lineEdit_mdpSWITCH->clear ();
}















//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur clique sur le radioButton "Erreur Destinataire"
  */
void MainWindow::on_radioButton_erreurDest_clicked(){

    ui->lineEdit_mdpSWITCH->setVisible (false);
    ui->lineEdit_mdpSWITCH->clear ();
}

















//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur clique sur le radioButton "Envoyer SEC"
  */
void MainWindow::on_radioButton_envoyerSEC_clicked(){

    ui->lineEdit_mdpSWITCH->setVisible (false);
    ui->lineEdit_mdpSWITCH->clear ();
}

















//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur clique sur le radioButton "Archivage NR"
  */
void MainWindow::on_radioButton_ArchivageNR_clicked(){

    ui->lineEdit_mdpSWITCH->setVisible (false);
    ui->lineEdit_mdpSWITCH->clear ();
}

























//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur coche la case Multi-s�lection
  */
void MainWindow::on_checkBox_multiSelection_clicked(){

    ecrireLog ( NOM_FICHIER_LOG, "Action -> Multi-s�lection");

    if( ui->checkBox_multiSelection->isChecked () )
        listerFichier ( pathMutuelleEnCours, true);
    else
        listerFichier ( pathMutuelleEnCours, false);

}























//----------------------------------------------------------------------------------------------------------------------
/*
  Permet de supprimer un fichier pass� en param�tre
  */
bool MainWindow::suppressionFichier (QString fichierASupprimer){

    QFile fichier( fichierASupprimer );

    if( !fichier.remove () ){

        debuggerInfo ("Suppression impossible","Impossible de supprimer le fichier " % fichierASupprimer
                      % " | Erreur : " % fichier.errorString () );

        return( false );
    }
    else
        return( true );

}




















//----------------------------------------------------------------------------------------------------------------------
bool MainWindow::deplacementFichier (QString dossier, QString pathFichier){

    QFile fichierPDF( pathFichier );
    QFileInfo infoFichier ( pathFichier );

    //Si le fichier PDF � d�placer existe
    if( fichierPDF.exists () ){

        //Si la copie n'a pas bien fonctionn�
        if( !fichierPDF.copy ( dossier % "/" % infoFichier.fileName ()) ){

            debuggerInfo ("Copie impossible TMP","Impossible de copier le fichier " % infoFichier.fileName ()
                          % " dans le dossier -> " % dossier % " | Erreur : " % fichierPDF.errorString () );
            return(false);
        }
        else{
            //Si la suppression du fichier d"origine n'a pas bien fonctionn�
            if( !fichierPDF.remove () ){

                debuggerInfo ("Suppression impossible TMP","Impossible de supprimer le fichier " % infoFichier.fileName ()
                              % " dans le dossier ERREUR | Erreur : " % fichierPDF.errorString () );
                return(false);
            }
            else{

                ecrireLog ("reconciliation.log","Le fichier " % infoFichier.fileName () % " a ete deplace dans "
                           % dossier);

                return(true);
            }
        }
    }
    else{

        debuggerInfo ("Fichier inconnu TMP","Le ou les fichiers s�lectionn�s ne sont pas dans le r�pertoire ERREUR");
        return(false);
    }
}
















//----------------------------------------------------------------------------------------------------------------------
/*
  Ferme le processus du lecteur PDF en fonction des pr�f�rences choisit par l'utilisateur
  */
void MainWindow::fermerFichierPDF(){

    QString lectuerPDF = getLecteurPDFDefaut();

    if( lectuerPDF == "SumatraPDF")
        system("taskkill /F /IM SumatraPDF.exe");
    else if( lectuerPDF == "adobeReader" ){

        system ("taskkill /F /IM AcroRd32.exe");
        Sleep(100);
    }
}



















//----------------------------------------------------------------------------------------------------------------------
/*
  Renvoi le lecteur par d�faut choisis (config.ini -> [LecteurPDF])
  */
QString MainWindow::getLecteurPDFDefaut(){

    QSettings settings(QCoreApplication::applicationDirPath () % "/config.ini", QSettings::IniFormat);
    QString lecteurPDF = settings.value ("LecteurPDF/Preference","Aucun").toString ();
    return( lecteurPDF );
}



















//----------------------------------------------------------------------------------------------------------------------
/*
  SLOT : Appelle une fen�tre dialogue pour choisir le lecteur de PDF par d�faut
  Cette pr�f�rence est utilis� pour la fermeture du PDF
  Si AdobeReader est coch� alors on fermera le processus AcroRd32.exe
  Si SumatraPDF est coch� alors on fermera le processus SumatraPDF.exe
  */
void MainWindow::on_actionLecteur_PDF_triggered(){

    Dialog_lecteurPDF *dial_lecteurPDF = new Dialog_lecteurPDF();
    dial_lecteurPDF->show ();

}














//----------------------------------------------------------------------------------------------------------------------
/*
  Permet de d�couper un fichier PDF s�lectionn�
  */
void MainWindow::on_btn_decouperPDF_clicked(){

//    debugger ("", "En d�veloppement");

    QProcess process;
    QStringList listeParam;
    listeParam << pathFichier << "burst" << "output" << "%05d_RPEC_" % QDateTime::currentDateTime ().toString ("yyyyMMdd_hhmmss") % ".pdf";
    process.start ("pdftk.exe", listeParam);
    process.waitForFinished (-1);

    debuggerInfo ("D�coupage PDF","Fichier d�coup�");
}












//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur active la check box "Tout"
  */
void MainWindow::on_checkBox_selectionnerTout_clicked(){

    if( ui->checkBox_selectionnerTout->isChecked () )
        remplirTableauCheckBox (listeDPEC, true);
    else
        remplirTableauCheckBox (listeDPEC, false);

}











//----------------------------------------------------------------------------------------------------------------------
/*
  Permet d'archiver dans le dossier NT
  */
void MainWindow::archiverNT(QString motif, bool siEnvoi){

    if( !motif.isEmpty () ){

        QFileInfo fichierInfo(pathFichier);
        QString nomFichierMotif = fichierInfo.baseName () % ".txt";

        Dial_bdd *bdd = new Dial_bdd(config->getPathINI () % "/" % hostname);
        //Met � jour les statistiques
        bdd->majDonnee ( config->getUserCourant (), "NBNT", 1);

        fermerFichierPDF();

        Sleep(100);

        //Enregistre le motif dans un fichier texte
        enregistrerMotif( motif );

        if( !siEnvoi )
            envoyerEmail( pathFichier, motif ); //envoi l'e-mail

        //D�placement fichier dans NT
        if ( deplacerFichier("NT", true) ){

            //Archiver motif
            archiverFichierMotif( fichierInfo.absolutePath () % "/" % nomFichierMotif );

            Dial_bdd *bdd_stats = new Dial_bdd(QCoreApplication::applicationDirPath () % "/Base_stats");

            bdd_stats->enregistrerReconciliation ( fichierInfo.fileName (),
                                                   ui->lineEdit_IDMsg->text (),
                                                   config->getUserCourant (),
                                                   hostname,
                                                   "NT",
                                                   ui->lineEdit_nomPatient->text (),
                                                   ui->lineEdit_prenomPatient->text (),
                                                   ui->lineEdit_numDossier->text (),
                                                   ui->lineEdit_dateEntree->text (),
                                                   ui->lineEdit_emetteur_DPEC->text (),
                                                   ui->lineEdit_dest_DPEC->text (),
                                                   mutuelleEnCours );

            debuggerInfo ("Archivage fichier","Archiv� dans NT");
            delete ui->listWidget_fichierPDF->currentItem ();

            viderWidget ();

            //Une fois qu'il n'y a plus de fichier dans le dossier
            if( ui->listWidget_fichierPDF->count () == 0 )
                QCoreApplication::quit ();
        }
        else
            debugger ("Archivage fichier","Fichier non d�plac�");
    }
    else
        debugger ("Motif NT","Veuillez s�lectionner un motif pour d�clarer votre non traitable");
}



















//----------------------------------------------------------------------------------------------------------------------
/*
  Permet de renseigner dans le fichier TEXTE le motif du non traitable
  */
void MainWindow::enregistrerMotif(QString libelleMotif){

    QFileInfo fichierInfo(pathFichier);
    QString nomFichierMotif = fichierInfo.baseName () % ".txt";

    QFile fichierMotif( fichierInfo.absolutePath () % "/" % nomFichierMotif );
    if( !fichierMotif.open ( QIODevice::Text | QIODevice::WriteOnly ) )
        debugger ("Ouverture/cr�ation fichier",
                  "Impossible d'ouvrir/cr�er le fichier " % fichierInfo.fileName () % ".txt" % " | Erreur : " % fichierMotif.errorString () );
    else{

        QTextStream fluxMotif(&fichierMotif);

        fluxMotif << libelleMotif << endl;

        fichierMotif.close ();
    }
}




















//----------------------------------------------------------------------------------------------------------------------
/*
  Archive le fichier motif du non traitable dans NT/DATE_JOUR
  */
void MainWindow::archiverFichierMotif(QString pathFichierMotif){

    QFileInfo fichierInfo( pathFichierMotif );
    QFile fichier( fichierInfo.absoluteFilePath () );

    QString nomFichierMotif = fichierInfo.fileName ();

    if( fichier.exists ( fichierInfo.absolutePath () % "/NT/" % QDate::currentDate ().toString ("yyyyMMdd") % "/" % nomFichierMotif ) )
        nomFichierMotif = nomFichierMotif % "_" % QDateTime::currentDateTime ().toString ("hhmmss") % ".txt";

    if( fichier.copy ( fichierInfo.absolutePath () % "/NT/" % QDate::currentDate ().toString ("yyyyMMdd") % "/" % nomFichierMotif ) ){

        if( fichier.remove () )
            debuggerInfo ("Motif", "Votre motif a �t� enregistr�, merci !");
        else
            debugger ("Suppression impossible",
                      "Impossible de supprimer le fichier " % nomFichierMotif % " | Erreur : " % fichier.errorString () );
    }
    else
        debugger ("Copie impossible",
                  "Impossible de Copier le fichier " % nomFichierMotif % " | Erreur : " % fichier.errorString () );
}
















//----------------------------------------------------------------------------------------------------------------------
/*
  Envoi un e-mail avec le fichier PDF et le motif dans le corps
  */
void MainWindow::envoyerEmail(QString pathPDF, QString motif){

    system("C:\\Postie\\postie.exe -host:mail.4axes.fr -from:poperocr@4axes.fr -to:rpec.nt@4axes.fr -s:\"Non traitable : "
           + mutuelleEnCours.toLocal8Bit () + "\" -msg:\"" + motif.toLocal8Bit () + "\ - " + hostname.toLocal8Bit ()
           + "\ - " + config->getUserCourant ().toLocal8Bit () + "\" -a:" + "\"" + pathPDF.toLocal8Bit () + "\"");

}


















//----------------------------------------------------------------------------------------------------------------------
/*
  Appelle le fen�tre "form_stats"
  */
void MainWindow::on_actionStatistiques_triggered(){

    Form_stats *stats = new Form_stats();
    stats->show ();
}














//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur souhaite mettre � jour l'API
  */
void MainWindow::on_actionMise_jour_triggered(){

    QProcess process;
    QStringList argument;
    argument << "standard";
    process.startDetached (QCoreApplication::applicationDirPath () % "/updater.exe", argument);
}











//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur clique sur "Importer autres mutuelles"
  */
void MainWindow::on_btn_importerAutresMutuelles_clicked(){

    siVmImporter = true;

    QString path = "\\\\" % config->getValue ( QCoreApplication::applicationDirPath () % "/config.ini",
                                        "VM", ui->comboBox_autresMutuelles->currentText () )
            % "/" % config->getValue ( QCoreApplication::applicationDirPath () % "/config.ini",
                                       "Path", "GoodNIN");

    QStringList listeDossier;

    QDirIterator iterateur( path, QDir::Dirs | QDir::NoSymLinks | QDir::NoDotAndDotDot);
    QDir dossier;

    while( iterateur.hasNext () ){

        dossier = iterateur.next ();
        listeDossier.push_back ( dossier.dirName () );
    }

    ui->comboBox_TMP->clear ();
    ui->comboBox_TMP->addItems ( listeDossier );
}
