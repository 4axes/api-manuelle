#include "dialog_lecteurpdf.h"
#include "ui_dialog_lecteurpdf.h"


#define ADOBE_READER        QString("AcroRd32.exe")
#define SUMATRA_PDF         QString("SumatraPDF.exe")





Dialog_lecteurPDF::Dialog_lecteurPDF(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog_lecteurPDF)
{
    ui->setupUi(this);

    initialiser ();
}

Dialog_lecteurPDF::~Dialog_lecteurPDF()
{
    delete ui;
}










//----------------------------------------------------------------------------------------------------------------------
/*
  Initialise les param�tres
  */
void Dialog_lecteurPDF::initialiser(){

    QSettings settings(QCoreApplication::applicationDirPath () % "/config.ini", QSettings::IniFormat);
    QString lecteurPDF = settings.value ("LecteurPDF/Preference", "Aucun").toString ();

    if( lecteurPDF == "SumatraPDF" )
        ui->radioButton_sumatraPDF->setChecked (true);

    else if( lecteurPDF == "adobeReader" )
        ui->radioButton_adobeReader->setChecked (true);
    else{

        ui->radioButton_adobeReader->setChecked (false);
        ui->radioButton_sumatraPDF->setChecked (false);
    }
}














//----------------------------------------------------------------------------------------------------------------------
/*
  SETTEUR : Modifie la valeur de la cl� "Preference
  */
void Dialog_lecteurPDF::setLecteurPDFDefaut(QString lecteurPDF){

    QSettings settings(QCoreApplication::applicationDirPath () % "/config.ini", QSettings::IniFormat);
    settings.beginGroup ("LecteurPDF");
    settings.setValue ("Preference", lecteurPDF);
}

















//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur valide le changement
  */
void Dialog_lecteurPDF::on_buttonBox_accepted(){

    if( ui->radioButton_sumatraPDF->isChecked () ){

        setLecteurPDFDefaut ( "SumatraPDF" );
        QMessageBox::information (NULL,"Changement valid�","Lecteur PDF modifi�");
    }
    else if( ui->radioButton_adobeReader->isChecked () ){

        setLecteurPDFDefaut ( "adobeReader" );
        QMessageBox::information (NULL,"Changement valid�","Lecteur PDF modifi�");
    }
    else if( ui->radioButton_sumatraPDF->isChecked () && ui->radioButton_adobeReader->isChecked () )
        QMessageBox::warning (NULL,"Validation lecteur PDF","Impossible d'avoir deux lecteurs PDF par d�faut");

    else
        QMessageBox::warning (NULL,"Validation lecteur PDF","Veuillez choisir un lecteur par d�faut");

}
