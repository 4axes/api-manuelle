#include "form_stats.h"
#include "ui_form_stats.h"




//----------------------------------------------------------------------------------------------------------------------
Form_stats::Form_stats(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Form_stats)
{
    ui->setupUi(this);
    this->setFixedSize (953,741);
    this->setWindowTitle ("Statistiques");

    ui->lineEdit_dateDebut->setVisible ( false );
    ui->lineEdit_dateFin->setVisible ( false );
    ui->label->setVisible ( false );

    ui->btn_exporter->setVisible ( false );

    clickID = 0;
}

Form_stats::~Form_stats()
{
    delete ui;
}











//----------------------------------------------------------------------------------------------------------------------
/*
  Permet d'afficher une information dans un QMessageBox
  */
void Form_stats::debugger(QString titre, QString information){

    QMessageBox::warning (NULL, titre, information);
}


















//----------------------------------------------------------------------------------------------------------------------
/*
  Permet de g�n�rer les statistiques d'une API [typeAPI] sur une journ�e donn�e [date]
  */
void Form_stats::genererStats(QString date, QString typeAPI, QString contexte){

    Dial_bdd *bdd = new Dial_bdd(QCoreApplication::applicationDirPath () % "/Base_Stats");
    QVector<double> stats = bdd->importerStats ( contexte, typeAPI, date );
    afficherGraphPoint ( stats );
}





















//----------------------------------------------------------------------------------------------------------------------
/*
  Permet de g�n�rer les statistiques d'une API [typeAPI] sur une p�riode donn�e [dateDebut/DateFin]
  */
void Form_stats::genererStats(QString dateDebut, QString dateFin, QString typeAPI, QString contexte){

    Dial_bdd *bdd = new Dial_bdd(QCoreApplication::applicationDirPath () % "/Base_Stats" );
    QVector<double> stats = bdd->importerStats ( contexte, typeAPI, dateDebut, dateFin );
    afficherGraphPoint ( stats );
}














//----------------------------------------------------------------------------------------------------------------------
/*
  SLOT : Lorsque l'utilisateur clique sur "G�n�rer statistiques"
  */
void Form_stats::on_btn_genererStats_clicked(){

    if( ui->checkBox_journee->isChecked () || ui->checkBox_periode->isChecked () ){

        if( ui->checkBox_journee->isChecked () && ui->checkBox_periode->isChecked () )
            debugger ("Double s�lection","Veuillez s�lectionner soit sur une journ�e soit sur une p�riode");
        else{

            if( ui->checkBox_journee->isChecked () && ui->lineEdit_dateDebut->text ().isEmpty () )
                debugger ("S�lection date", "Veuillez s�lectionner une date dans votre crit�re de recherche par journ�e");

            else if( ui->checkBox_periode->isChecked () && (ui->lineEdit_dateDebut->text ().isEmpty () || ui->lineEdit_dateFin->text ().isEmpty () ) )
                     debugger ("S�lection date","Veuillez s�lectionner deux dates pour votre crit�re de recherche par p�riode");

            else{

                ui->btn_exporter->setVisible ( true );

                if( ui->radioButton_rechercheAuto->isChecked () || ui->radioButton_rechercheManu->isChecked ()
                        || ui->radioButton_reconciliationAuto->isChecked () || ui->radioButton_reconciliationManu->isChecked () ){

                    QDate dateDebut;
                    QDate dateFin;

                    QString dateD = dateDebut.fromString (ui->lineEdit_dateDebut->text (), "dd/MM/yyyy").toString ("yyyyMMdd");
                    QString dateF = dateFin.fromString (ui->lineEdit_dateFin->text (), "dd/MM/yyyy").toString ("yyyyMMdd");

                    if( ui->radioButton_rechercheAuto->isChecked () ){          //API auto -> recherche

                        contexte = "API automatique : Recherche";

                        if( ui->checkBox_journee->isChecked () )
                            genererStats( dateD, "auto", "recherche");

                        else if ( ui->checkBox_periode->isChecked () )
                            genererStats( dateD, dateF, "auto", "recherche");

                    }
                    else if ( ui->radioButton_rechercheManu->isChecked () ){    //API manuelle -> recherche

                        contexte = "API manuelle : Recherche";

                        if( ui->checkBox_journee->isChecked () )
                            genererStats( dateD, "manuelle", "recherche");

                        else if ( ui->checkBox_periode->isChecked () )
                            genererStats(dateD , dateF, "manuelle", "recherche");
                    }
                    else if( ui->radioButton_reconciliationAuto->isChecked () ){    //API auto -> envoiFichier

                        contexte = "API automatique : envoi de fichier";

                        if( ui->checkBox_journee->isChecked () )
                            genererStats( dateD, "auto", "envoiFichier");

                        else if ( ui->checkBox_periode->isChecked () )
                            genererStats(dateD, dateF, "auto", "envoiFichier");
                    }
                    else if( ui->radioButton_reconciliationManu->isChecked () ){    //API manuelle -> envoiFichier

                        contexte = "API manuelle : envoi de fichier";

                        if( ui->checkBox_journee->isChecked () )
                            genererStats( dateD, "manuelle", "envoiFichier");

                        else if ( ui->checkBox_periode->isChecked () )
                            genererStats( dateD, dateF, "manuelle", "envoiFichier");
                    }
                }
                else
                    debugger ("S�lection API","Veuillez cocher le type d'API pour g�n�rer vos statistiques");
            }
        }
    }
    else
        debugger ("S�lection journ�e/p�riode","Veuillez s�lectionner une votre crit�re de recherche [Journ�e/P�riode]");
}


























//----------------------------------------------------------------------------------------------------------------------
/*
  Permet d'afficher les donn�es sur le graphique
  */
void Form_stats::afficherGraphPoint (QVector<double> vecteurY){

    QVector<double> x( vecteurY.size () );

    moyenne = 0;

    for (int i = 0; i < vecteurY.size () ; i++){

        moyenne += vecteurY.at (i);
        x[i] = i+1;
    }

    moyenne = moyenne / vecteurY.size ();
    ui->lineEdit_moyenne->setText ( QString::number ( moyenne ) % "s");


    int lastValue = vecteurY.at ( vecteurY.size () - 1 );
    int maxValue = lastValue;

    for(int j = 0 ; j < vecteurY.size () ; j++){

        if( maxValue < vecteurY.at (j) )
            maxValue = vecteurY.at (j);
    }


    // create graph and assign data to it:
    ui->widget_stats->addGraph();
    ui->widget_stats->graph(0)->setData(x, vecteurY);

    //Legend
    ui->widget_stats->xAxis->setLabel("Nombre de r�conciliation");
    ui->widget_stats->yAxis->setLabel("Temps de r�conciliation");

    // set axes ranges, so we see all data:
    ui->widget_stats->xAxis->setRange(0, vecteurY.size ());
    ui->widget_stats->yAxis->setRange(0, maxValue + 5);
    ui->widget_stats->replot();
}










































//----------------------------------------------------------------------------------------------------------------------
void Form_stats::on_calendrier_clicked(const QDate &date){

    if( ui->checkBox_journee->isChecked () | ui->checkBox_periode->isChecked () ){

        //Si journ�e activ�e
        if( ui->checkBox_journee->isChecked () )
            ui->lineEdit_dateDebut->setText ( date.toString ("dd/MM/yyyy") );

        //Si p�riode activ�e
        else if( ui->checkBox_periode->isChecked () ){

            //Si aucun clique n'a �t� fait
            if( clickID == 0 ){

                ui->lineEdit_dateDebut->setText ( date.toString ("dd/MM/yyyy") );
                clickID = 1;
            }
            //Lorsqu'un clique a d�j� �t� fait
            else if( clickID == 1 ){

                ui->lineEdit_dateFin->setText ( date.toString ("dd/MM/yyyy") );
                clickID = 0;
            }
        }
    }
    else
        debugger ("S�lection crit�re","Veuillez s�lectionner une journ�e ou une p�riode avant de s�lectionner une date");

}


















//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur coche "Journ�e"
  */
void Form_stats::on_checkBox_journee_clicked(bool checked){

    ui->checkBox_periode->setVisible ( !checked );
    ui->lineEdit_dateDebut->setVisible ( checked );
    ui->lineEdit_dateDebut->setText ( QDate::currentDate ().toString ("dd/MM/yyyy"));
}















//----------------------------------------------------------------------------------------------------------------------
/*
  Lorsque l'utilisateur coche "P�riode"
  */
void Form_stats::on_checkBox_periode_clicked(bool checked)
{
    ui->checkBox_journee->setVisible ( !checked );
    ui->lineEdit_dateDebut->setVisible ( checked );
    ui->lineEdit_dateFin->setVisible ( checked );
    ui->label->setVisible ( checked );
}

















//----------------------------------------------------------------------------------------------------------------------
/*
  Permet d'exporter les statistiques
  */
void Form_stats::on_btn_exporter_clicked(){

    QHostInfo host;
    QString hostName = host.localHostName ();
    QString nameFichierExport = QCoreApplication::applicationDirPath () % "/export_" % hostName % ".pdf" ;

    if( ui->widget_stats->savePdf ( nameFichierExport,1000,500) ){

        system("C:\\Postie\\postie.exe -host:mail.4axes.fr -from:stats@4axes.fr -to:statsvms@4axes.fr -s:\"Statistiques de "
               + hostName.toLocal8Bit () + " - " + contexte.toLocal8Bit () + "\" -msg:\"Moyenne (seconde) : \"" + (QString::number ( moyenne )).toLocal8Bit ()
               + " -a:" + "\"" + nameFichierExport.toLocal8Bit () + "\"");

        QMessageBox::information (NULL,"Export statistique", "Statistiques export�es et envoy�es pour contr�le");
    }
    else
        debugger ("Erreur exportation statistique","Impossible d'exporter les statistiques");
}
